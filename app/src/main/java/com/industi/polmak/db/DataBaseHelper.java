package com.industi.polmak.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by alexb on 19.09.2016.
 * <p/>
 * Class which handles all local database operations. Contains tables SERIES, SHAPES, DIFFICULTY. CATEGORIES and IMAGES.
 * With its help you can add, read and delete records from tables.
 */

public class DataBaseHelper extends SQLiteOpenHelper {
    //description as a variables names
    private static final String DATABASE_NAME = "polmak_db";
    private static final int DATABASE_VERSION = 5 ;

    //db tables names
    private final String TABLE_LIKES = "likes";
    private final String TABLE_RECIPE_INGRREDIENTS = "recipes_ingredients";
    //private final String TABLE_SERIES = "series";
    //private final String TABLE_SHAPES = "shapes";
    //private final String TABLE_CATEGORIES = "categories";
    //private final String TABLE_IMAGES = "images";

    //tables key names
    private final String KEY_RECIPE_ID = "recipe_id";
    private final String KEY_INGREDIENTS = "ingredients";
    //private final String KEY_IS_LIKE = "is_like";
    //private final String KEY_IMAGE_LINK = "image_link";
    //private final String KEY_IMAGE = "image";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //create requests to db
        String CREATE_LIKES_TABLE = "CREATE TABLE " + TABLE_LIKES + "(" + KEY_RECIPE_ID + " INTEGER PRIMARY KEY" + ")";
        String CREATE_INGREDIENTS_TABLE = "CREATE TABLE " + TABLE_RECIPE_INGRREDIENTS + "(" + KEY_RECIPE_ID + " INTEGER PRIMARY KEY," + KEY_INGREDIENTS + " TEXT)";
        sqLiteDatabase.execSQL(CREATE_LIKES_TABLE);
        sqLiteDatabase.execSQL(CREATE_INGREDIENTS_TABLE);
        /*String CREATE_SERIES_TABLE = "CREATE TABLE " + TABLE_SERIES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT" + ")";
        String CREATE_SHAPES_TABLE = "CREATE TABLE " + TABLE_SHAPES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT," + KEY_IMAGE_LINK + " TEXT" + ")";
        String CREATE_CATEGORIES_TABLE = "CREATE TABLE " + TABLE_CATEGORIES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT" + ")";
        String CREATE_IMAGES_TABLE = "CREATE TABLE " + TABLE_IMAGES + "("
                + KEY_IMAGE_LINK + " TEXT," + KEY_IMAGE + " BLOB" + ")";

        //execute requests - create tables
        sqLiteDatabase.execSQL(CREATE_SERIES_TABLE);
        sqLiteDatabase.execSQL(CREATE_SHAPES_TABLE);
        sqLiteDatabase.execSQL(CREATE_CATEGORIES_TABLE);
        sqLiteDatabase.execSQL(CREATE_IMAGES_TABLE);*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //delete all tables
        /*sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_SERIES);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_SHAPES);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIES);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_IMAGES);*/
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_LIKES);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_RECIPE_INGRREDIENTS);
        //create tables
        onCreate(sqLiteDatabase);
    }

    public boolean isRecipeLiked(int recipeId) {
        String query = "SELECT * FROM " + TABLE_LIKES + " WHERE " + KEY_RECIPE_ID + " = " + recipeId;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            int id = cursor.getInt(0);
            if (id == recipeId) return true;
        }
        return false;
    }

    public void addLike(int recipeId) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(KEY_RECIPE_ID, recipeId);
        database.insert(TABLE_LIKES, null, cv);
        database.close();
    }

    public void deleteLike(int recipeId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_LIKES + " WHERE " + KEY_RECIPE_ID + " = " + recipeId);
        db.close();
    }

    public ArrayList<Integer> getLikes() {
        String query = "SELECT * FROM " + TABLE_LIKES;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<Integer> likes = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                likes.add(cursor.getInt(0));
            } while (cursor.moveToNext());
        }
        return likes;
    }

    public void addIngredients(int recipeId, String ingredients){
        print();
        deleteIngredients(recipeId);
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(KEY_RECIPE_ID, recipeId);
        cv.put(KEY_INGREDIENTS, ingredients);
        database.insert(TABLE_RECIPE_INGRREDIENTS, null, cv);
        database.close();
        print();
    }

    public void deleteIngredients(int recipeId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_RECIPE_INGRREDIENTS + " WHERE " + KEY_RECIPE_ID + " = " + recipeId);
        db.close();
    }

    public String getIngredientsById(int recipeId){
        String query = "SELECT * FROM " + TABLE_RECIPE_INGRREDIENTS + " WHERE " + KEY_RECIPE_ID + " = " + recipeId;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            return cursor.getString(1);
        }
        return "";
    }

    private void print(){
        String query = "SELECT * FROM " + TABLE_RECIPE_INGRREDIENTS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Log.i("INGREDIENT ", cursor.getInt(0) + " " + cursor.getString(1));
            } while (cursor.moveToNext());
        }
    }

    /**
     * Add a new image to db
     *
     * @param imageLink image link
     * @param image     byte array of the image
     * @throws SQLiteException
     */
    /*public void addImage(String imageLink, byte[] image) throws SQLiteException {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(KEY_IMAGE_LINK, imageLink);
        cv.put(KEY_IMAGE, image);
        database.insert(TABLE_IMAGES, null, cv);
        database.close();
    }*/

    /**
     * Add list of series to db
     *
     * @param series list of series to add
     */
    /*public void addSeries(ArrayList<Series> series) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues cv;
        for (Series s : series) {
            cv = new ContentValues();
            cv.put(KEY_ID, s.getId());
            cv.put(KEY_NAME, s.getName());
            database.insert(TABLE_SERIES, null, cv);
        }
        database.close();
    }
*/
    /**
     * Add list of shapes to db
     *
     * @param shapes list of shapes to add
     */
    /*public void addShapes(ArrayList<Shape> shapes) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues cv;
        for (Shape s : shapes) {
            cv = new ContentValues();
            cv.put(KEY_ID, s.getId());
            cv.put(KEY_NAME, s.getName());
            cv.put(KEY_IMAGE_LINK, s.getImageLink());
            database.insert(TABLE_SHAPES, null, cv);
        }
        database.close();
    }*/

    /**
     * Add list of categories to db
     *
     * @param categories list of categories to add
     */
    /*public void addCategory(ArrayList<RecipeCategory> categories) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues cv;
        for (RecipeCategory s : categories) {
            cv = new ContentValues();
            cv.put(KEY_ID, s.getId());
            cv.put(KEY_NAME, s.getName());
            database.insert(TABLE_CATEGORIES, null, cv);
        }
        database.close();
    }*/

    /**
     * Returns image by its image link
     *
     * @param imageLink image link
     * @return bitmap
     */
    /*public Bitmap getImageByLink(String imageLink) {
        String query = "SELECT " + KEY_IMAGE + " FROM " + TABLE_IMAGES + " WHERE " + KEY_IMAGE_LINK + " = " + imageLink;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Log.i("CURSOR", cursor.toString());
        byte[] imageArr = null;
        if (cursor.moveToFirst()) {
            imageArr = cursor.getBlob(0);
        }
        return DBBitmapUtility.getImage(imageArr);
    }

    public ArrayList<Series> getSeries() {
        String query = "SELECT * FROM " + TABLE_SERIES;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<Series> list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(0);
                String name = cursor.getString(1);
                list.add(new Series(id, name));
            } while (cursor.moveToNext());
        }
        return list;
    }

    public ArrayList<Shape> getShapes() {
        String query = "SELECT * FROM " + TABLE_SHAPES;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<Shape> list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(0);
                String name = cursor.getString(1);
                String link = cursor.getString(2);
                list.add(new Shape(id, name, link));
            } while (cursor.moveToNext());
        }
        return list;
    }

    public ArrayList<RecipeCategory> getCategories() {
        String query = "SELECT * FROM " + TABLE_CATEGORIES;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<RecipeCategory> list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(0);
                String name = cursor.getString(1);
                list.add(new RecipeCategory(id, name));
            } while (cursor.moveToNext());
        }
        return list;
    }

    public Shape getShapeById(int id) {
        String query = "SELECT * FROM" + TABLE_SHAPES + " WHERE " + KEY_ID + " = " + id;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Log.i("CURSOR", cursor.toString());
        if (cursor.moveToFirst()) {
            int id1 = cursor.getInt(0);
            String name = cursor.getString(1);
            String link = cursor.getString(2);
            return new Shape(id1, name, link);
        }
        return null;
    }*/

}
