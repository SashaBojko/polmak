package com.industi.polmak.requests;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.industi.polmak.basic.Ingredient;
import com.industi.polmak.basic.Pasta;
import com.industi.polmak.basic.Recipe;
import com.industi.polmak.basic.RecipeCategory;
import com.industi.polmak.basic.Series;
import com.industi.polmak.basic.Shape;
import com.industi.polmak.controllers.ApplicationController;
import com.industi.polmak.controllers.DataMediator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by alexb on 13.09.2016.
 */

public class GetRequest {

    public static final int GET_PASTAS = 1;
    public static final int GET_RECIPES = 2;
    public static final int GET_SERIES = 3;
    public static final int GET_RECIPE_CATEGORIES = 4;
    public static final int GET_SHAPES = 5;

    private String URL;
    private int type;

    //success - makes true then response got successfully
    //error   -            -/-           got error
    private boolean error, success;

    public GetRequest(Context context, String url, int type) {
        this.URL = url;
        this.type = type;

        error = false;
        success = false;

        read();
    }

    private void read() {
        StringRequest getRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        parseData(response);

                        //if will be token - use this check
                        /*if (checkResponse(response)) {
                            parseData(response);
                        } else {
                            error = true;
                        }*/
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("RESPONSE", "GETPASTAS  ERROR");
                        GetRequest.this.error = true;
                    }
                }
        ) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Basic " + Token.getTOKEN(context));
                return params;
            }*/
        };
        ApplicationController.getInstance().addToRequestQueue(getRequest);
    }

    private void parseData(String response) {
        switch (type) {
            case GET_PASTAS:
                parsePastasResponse(response);
                break;
            case GET_RECIPES:
                parseRecipesResponse(response);
                break;
            case GET_RECIPE_CATEGORIES:
                parseRecipeCategoriesResponse(response);
                break;
            case GET_SERIES:
                parseSeriesResponse(response);
                break;
            case GET_SHAPES:
                parseShapesResponse(response);
                break;
        }
    }

    private void parsePastasResponse(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject pasta = jsonArray.getJSONObject(i);
                int id = pasta.getInt("Id");
                String name = pasta.getString("Name");
                String imageLink = pasta.getString("Image");
                int shape = pasta.getInt("Shape");
                int series = pasta.getInt("Series");
                int alDenteTime = pasta.getInt("AlDenteTime");
                int boiledTime = pasta.getInt("BoiledTime");
                String ean = pasta.getString("EAN");

                JSONArray grammageJSON = pasta.getJSONArray("Grammage");
                JSONObject grammage = grammageJSON.getJSONObject(0);
                int weight = grammage.getInt("Type");

                JSONArray ingredientsJSON = pasta.getJSONArray("Ingredients");
                Ingredient ingredientsArray[] = new Ingredient[3];
                for (int j = 0; j < ingredientsJSON.length(); j++) {
                    JSONObject ingredient = ingredientsJSON.getJSONObject(j);
                    ingredientsArray[j] = new Ingredient(ingredient.getInt("Id"), ingredient.getString("Type"), ingredient.getString("Image"));
                }

                DataMediator.addPasta(new Pasta(id, name, imageLink, shape, series, alDenteTime, boiledTime, weight, ean, ingredientsArray));
            }
            success = true;

        } catch (JSONException e) {
            e.printStackTrace();
            error = true;
        }
    }

    private void parseRecipesResponse(String response) {
        try {
            JSONArray array = new JSONArray(response);
            for (int i = 0; i < array.length(); i++) {
                JSONObject recipe = array.getJSONObject(i);
                int id = recipe.getInt("Id");
                String title = recipe.getString("Title");
                String description = recipe.getString("Description");
                String preparation = recipe.getString("Preparation");
                String ingredients = recipe.getString("Ingredients");
                String imageLink = recipe.getString("Image");
                int time = recipe.getInt("Time");
                int energyValue = recipe.getInt("EnergyValue");
                int price = recipe.getInt("Price");
                int pastaId = recipe.getInt("Pasta");
                int category = recipe.getInt("Category");
                int difficulty = recipe.getInt("Difficulty");

                ArrayList<Ingredient> ingredientsList = new ArrayList<>();
                String stringList[] = ingredients.split(",");
                for (int j = 0; j < stringList.length; j++) {
                    ingredientsList.add(new Ingredient(stringList[j]));
                }

                DataMediator.addRecipe(new Recipe(id, imageLink, title, preparation, description, ingredientsList, time, energyValue, price, pastaId, category, difficulty));
            }
            success = true;
        } catch (JSONException e1) {
            e1.printStackTrace();
            error = true;
        }
    }

    private void parseSeriesResponse(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject pasta = jsonArray.getJSONObject(i);
                int id = pasta.getInt("Id");
                String name = pasta.getString("Type");

                DataMediator.addSeries(new Series(id, name));
            }
            success = true;

        } catch (JSONException e) {
            e.printStackTrace();
            error = true;
        }
    }

    private void parseRecipeCategoriesResponse(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject pasta = jsonArray.getJSONObject(i);
                int id = pasta.getInt("Id");
                String name = pasta.getString("Type");

                DataMediator.addRecipeCategory(new RecipeCategory(id, name));
            }
            success = true;

        } catch (JSONException e) {
            e.printStackTrace();
            error = true;
        }
    }

    private void parseShapesResponse(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject shape = jsonArray.getJSONObject(i);
                int id = shape.getInt("Id");
                String type = shape.getString("Type");
                String imageLink = shape.getString("Image");

                DataMediator.addShape(new Shape(id, type, imageLink));
            }
            success = true;

        } catch (JSONException e) {
            e.printStackTrace();
            error = true;
        }
    }

    /*private boolean checkResponse(String response) {
        if (response.contains("Unauthorized")) return false;
        return true;
    }*/

    public boolean isError() {
        //Log.i("ISERROR", type + " " + error);
        return error;
    }

    public boolean isSuccess() {
        //Log.i("ISSUCCESS", type + " " + success);
        return success;
    }
}
