package com.industi.polmak.requests;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by alexb on 10.10.2016.
 */

public class ImageLoader {
    private ContextWrapper cw;
    private File directory, imageFile;
    private Context context;

    public ImageLoader(Context context){
        cw = new ContextWrapper(getApplicationContext());
        directory = cw.getDir("images", Context.MODE_PRIVATE);
        this.context = context;
    }

    private Target getImage(Context context, final String imageName) {
        ContextWrapper cw = new ContextWrapper(context);
        //final File directory = cw.getDir("images", Context.MODE_PRIVATE); // path to /data/data/yourapp/app_images
        if(!directory.exists()){
            directory.mkdir();
        }

        return new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final File myImageFile = new File(directory, imageName); // Create image file
                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(myImageFile);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        //Log.i("image", "image saved to >>>" + myImageFile.getAbsolutePath());
                    }
                }).start();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }
            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                if (placeHolderDrawable != null) {}
            }
        };
    }

    public void setImage(ImageView holder, String imageLink){
        imageFile = new File(directory, imageLink.hashCode() + ".png");
        if (!imageFile.exists()) {
            //Log.i("IMAGE", "NOT EXISTS");
            Picasso.with(holder.getContext()).load(Uri.parse(imageLink)).into(getImage(context, imageLink.hashCode() + ".png"));
            Picasso.with(holder.getContext()).load(Uri.parse(imageLink)).into(holder);
        }else{
            Picasso.with(holder.getContext()).load(imageFile).into(holder);
        }
    }
}