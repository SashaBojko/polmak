package com.industi.polmak.requests;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by alexb on 08.08.2016.
 */

public class Token {
    private static String TOKEN;

    public static void setTOKEN(String token, Context context){
        TOKEN = token;
        addTokenToSharePreferences(context);
    }

    public static String getTOKEN(Context context) {
        if(TOKEN == null) getTokenFromSharePreferences(context);
        return TOKEN;
    }

    private static void addTokenToSharePreferences(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("token", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("token", getTOKEN(context));
        editor.apply();
    }

    private static void getTokenFromSharePreferences(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("token", Context.MODE_PRIVATE);
        TOKEN = sharedPreferences.getString("token", null);
        //if(TOKEN==null) new PostSignIn(context);
    }
}
