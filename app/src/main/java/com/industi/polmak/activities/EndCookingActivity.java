package com.industi.polmak.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.RelativeLayout;

import com.facebook.share.widget.LikeView;
import com.industi.polmak.R;

/**
 * Information activity, which calls when user finished cooking his pasta.
 */


public class EndCookingActivity extends AppCompatActivity {

    //button to close activity and back to the MainActivity
    private RelativeLayout layout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.end_cooking_activity);

        layout = (RelativeLayout) findViewById(R.id.end_cooking_layout);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        LikeView likeView = (LikeView) findViewById(R.id.likeView);
        likeView.setLikeViewStyle(LikeView.Style.STANDARD);
        likeView.setAuxiliaryViewPosition(LikeView.AuxiliaryViewPosition.INLINE);
        likeView.setObjectIdAndType(
                "https://www.facebook.com/RealMadrid",
                LikeView.ObjectType.OPEN_GRAPH);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //delete action bar elevation to
        getSupportActionBar().setElevation(0);

        //hide back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        //without tittle
        getSupportActionBar().setTitle("");
        return super.onCreateOptionsMenu(menu);
    }

}
