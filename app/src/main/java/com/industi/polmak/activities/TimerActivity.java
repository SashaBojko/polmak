package com.industi.polmak.activities;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.industi.polmak.R;
import com.industi.polmak.controllers.ApplicationController;
import com.industi.polmak.controllers.DataMediator;
import com.industi.polmak.enums.URLS;
import com.industi.polmak.fragments.NoodleTimerFragment;
import com.industi.polmak.fragments.NoodleTimerRingOGLRenderer;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexb on 09.09.2016.
 * <p>
 * Timer activity, with help of which, user can cook his pasta in the right way. Opens when user select some pasta from PastaDetailsActivity.
 * Implements NoodleTimerFragment.
 */

public class TimerActivity extends AppCompatActivity implements NoodleTimerFragment.OnFragmentInteractionListener {
    /**
     * Possible states of the elements.
     */
    //before pressing start button
    private final int ENABLE_MODE_BEFORE_START = 0;
    //after pressing start button
    private final int ENABLE_MODE_ON_START = 1;
    //after pressing pause button
    private final int ENABLE_MODE_ON_PAUSE = 2;
    //after pressing resume button
    private final int ENABLE_MODE_ON_RESUME = 3;
    //after timer finished
    private final int ENABLE_MODE_FINISHED = 4;

    //info text view inside the clock (Gotujesz juz)
    private TextView clockInfoTextView;
    //optimal time minutes text view
    private TextView minutes;
    //optimal time seconds text view
    private TextView seconds;
    //AlDente time minutes text view
    private TextView alDenteMinutes;
    //AlDente time seconds text view
    private TextView alDenteSeconds;
    //communicate textViews
    private TextView firstInfoTv;
    private TextView secondInfotv;
    //textView to show name of selected pasta
    private TextView pastaName;
    //easy
    private Button startButton;

    private LinearLayout stopButton;
    //layout with alDente timer
    private LinearLayout alDenteTimeLayout;

    private ProgressDialog progressDialog;

    //selected pasta ID
    private int pastaId;

    //optimal pasta boiled time
    private int boiledTime;
    //alDente pasta boiled time
    private int alDenteTime;


    private int afterElapsedTimeNotification = 0;

    //I think it's simple
    private boolean isStarted = false;
    private boolean isPaused = false;
    private boolean isAlDenteFinished = false;
    private boolean isFinished = false;

    //fragment manager to start timer fragment
    private FragmentManager fragMan;
    //timer fragment
    private static NoodleTimerFragment noodleTimerFragment;
    private NoodleTimerRingOGLRenderer currentRenderer;
    private NotificationManager notificationManager;
    private final int NOTIFICATION_ID = 100;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timer_activity);

        getExtras();
        //initialize view elements

        initButtons();
        initTextViews();
        initFragment();
        initProgressDialog();
        //get extras from previous activity
        initTimerFragment();
        //set elements states
        enabledElements(ENABLE_MODE_BEFORE_START);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //delete action bar elevation
        getSupportActionBar().setElevation(0);
        //show back arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //simple
        getSupportActionBar().setTitle("Wróć");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //back arrow in toolbar
            //shows in some of activities
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!isStarted) {
            if (notificationManager != null) {
                notificationManager.cancel(NOTIFICATION_ID);
            }
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (notificationManager != null) {
            notificationManager.cancel(NOTIFICATION_ID);
        }
        Log.i("ONRESUME", "LL");
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (notificationManager != null) {
            notificationManager.cancel(NOTIFICATION_ID);
        }
        Log.i("ONSTOP", "LL");
    }

    private void initTimerFragment() {
        GLSurfaceView renderViewport = (GLSurfaceView) findViewById(R.id.oglViewport);
        if (pastaId != -1) {
            currentRenderer = new NoodleTimerRingOGLRenderer(
                    new float[]{1.0f, 51f / 255f, 51f / 255f, 1.0f},
                    new float[]{0.5f, 0.0f, 0.0f, 1.0f},
                    new float[]{1.0f, 1.0f, 1.0f, 1.0f},
                    new float[]{1.0f, 1.0f, 1.0f, 1.0f},
                    new float[]{1.0f, 1.0f, 0.0f, 1.0f},
                    DataMediator.getPastaById(pastaId).getAlDenteTime() * 60,
                    DataMediator.getPastaById(pastaId).getBoiledTime() * 60
            );
            renderViewport.setRenderer(currentRenderer);
        }else{
            currentRenderer = new NoodleTimerRingOGLRenderer(
                    new float[]{1.0f, 51f / 255f, 51f / 255f, 1.0f},
                    new float[]{0.5f, 0.0f, 0.0f, 1.0f},
                    new float[]{1.0f, 1.0f, 1.0f, 1.0f},
                    new float[]{1.0f, 1.0f, 1.0f, 1.0f},
                    new float[]{1.0f, 1.0f, 0.0f, 1.0f},
                    1,
                    1
            );
            renderViewport.setRenderer(currentRenderer);
        }
    }

    private void initButtons() {
        stopButton = (LinearLayout) findViewById(R.id.timer_stop);
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                //simple
                getSupportActionBar().setTitle("Wróć");
                //reset timer
                noodleTimerFragment.stopAllTimersGetElapsed();
                //noodleTimerFragment.resetAllTimers();
                //Reset all state flags
                isPaused = false;
                isStarted = false;
                isFinished = true;
                startButton.setText(R.string.write_time_and_finish);
                //set elements states
                enabledElements(ENABLE_MODE_FINISHED);
            }
        });

        startButton = (Button) findViewById(R.id.timer_done_button);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //if timer is finished
                if (isFinished) {
                    //open end-cooking info activity
                    noodleTimerFragment.stopAllTimersGetElapsed();
                    sendTimeToServer();
                    return;
                }
                //if timer hasn't been started
                if (!isStarted) {
                    if (pastaId == -1) {
                        onBackPressed();
                        return;
                    }
                    //set times to timer and start
                    noodleTimerFragment.addAlDenteTimer(DataMediator.getPastaById(pastaId).getAlDenteTime() * 60);
                    noodleTimerFragment.addOptimalTimer(DataMediator.getPastaById(pastaId).getBoiledTime() * 60);
                    noodleTimerFragment.startAllTimers();
                    //set start flag
                    isStarted = true;
                    //set elements states
                    enabledElements(ENABLE_MODE_ON_START);
                    return;
                }

                if (isStarted && !isPaused) {
                    noodleTimerFragment.pauseAllTimers();
                    //set pause flag
                    isPaused = true;
                    //set elements states
                    enabledElements(ENABLE_MODE_ON_PAUSE);
                    return;
                }
                //if timer is on pause
                if (isPaused) {
                    noodleTimerFragment.resumeAllTimers();
                    isPaused = !isPaused;
                    enabledElements(ENABLE_MODE_ON_RESUME);
                }

            }
        });
        alDenteTimeLayout = (LinearLayout) findViewById(R.id.timer_left_time_layout);
    }

    /**
     * TextViews initialization
     */
    private void initTextViews() {
        clockInfoTextView = (TextView) findViewById(R.id.timer_clock_info_tv);
        minutes = (TextView) findViewById(R.id.timer_minutes);
        seconds = (TextView) findViewById(R.id.timer_seconds);
        alDenteMinutes = (TextView) findViewById(R.id.timer_minutes_left);
        alDenteSeconds = (TextView) findViewById(R.id.timer_seconds_left);
        firstInfoTv = (TextView) findViewById(R.id.timer_first_tv_info);
        secondInfotv = (TextView) findViewById(R.id.timer_second_tv_info);
        pastaName = (TextView) findViewById(R.id.timer_pasta_name);
    }

    /**
     * Getting selected pasta ID from previous activity
     */
    private void getExtras() {
        Intent intent = getIntent();
        pastaId = intent.getIntExtra("pastaId", -1);
    }

    /**
     * Timer fragment and fragment manager initialization
     */

    private void initFragment() {
        fragMan = getSupportFragmentManager();
        FragmentTransaction timerTransaction = fragMan.beginTransaction();
        noodleTimerFragment = NoodleTimerFragment.newInstance();
        noodleTimerFragment.resetAllTimers();
        timerTransaction.add(noodleTimerFragment, "noodleTimerInstance");
        timerTransaction.commit();
    }

    /**
     * Set elements visibility and data depending on the mode.
     *
     * @param mode mode to set enable elements
     */
    private void enabledElements(int mode) {
        switch (mode) {
            case ENABLE_MODE_BEFORE_START: {
                //if pasta hasn't been selected (pressed timer floating action button)
                if (pastaId != -1) {
                    //get boilesd and alDente time from DataMediator controller
                    boiledTime = DataMediator.getPastaById(pastaId).getBoiledTime();
                    alDenteTime = DataMediator.getPastaById(pastaId).getAlDenteTime();
                    //set pasta name
                    pastaName.setText(DataMediator.getPastaById(pastaId).getName());
                    //set communicates to info textViews
                    firstInfoTv.setText(R.string.is_water_is_boiled);
                    secondInfotv.setText(R.string.put_in_pasta_end_press_start);
                    startButton.setText("Start");
                }
                //pasta was selected
                else {
                    //set default times
                    alDenteTime = 0;
                    boiledTime = 0;
                    //set pasta name invisible
                    pastaName.setVisibility(View.INVISIBLE);
                    startButton.setText("Wróć");
                    //disenable start button, cause pasta hasn't been selected
                    //show communicate to select pasta
                    firstInfoTv.setText(R.string.select_pasta_first);
                    secondInfotv.setText("");
                }
                minutes.setText(String.valueOf(boiledTime));
                seconds.setText("00");
                alDenteMinutes.setText(String.valueOf(alDenteTime));
                alDenteSeconds.setText("00");


                clockInfoTextView.setVisibility(View.INVISIBLE);
                alDenteTimeLayout.setVisibility(View.INVISIBLE);
                stopButton.setVisibility(View.INVISIBLE);
                break;
            }
            case ENABLE_MODE_ON_START: {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setTitle("");
                firstInfoTv.setText("");
                secondInfotv.setText("");
                startButton.setText("Pauza");
                clockInfoTextView.setVisibility(View.VISIBLE);
                alDenteTimeLayout.setVisibility(View.VISIBLE);
                stopButton.setVisibility(View.INVISIBLE);
                break;
            }
            case ENABLE_MODE_ON_PAUSE:
                stopButton.setVisibility(View.VISIBLE);
                startButton.setText("Kontynuuj");
                break;
            case ENABLE_MODE_ON_RESUME:
                startButton.setText("Pauza");
                stopButton.setVisibility(View.INVISIBLE);
                break;
            case ENABLE_MODE_FINISHED:
                stopButton.setVisibility(View.INVISIBLE);
                break;
        }
    }

    /**
     * 0 - alDenteFinished notification
     * 1 - optimalFinished notification
     *
     * @param notificationType type of notification
     */
    private void showNotification(int notificationType) {
        String ticker = "", tittle = "", text = "";
        switch (notificationType) {
            case 0:
                ticker = getResources().getString(R.string.aldente_done);
                tittle = getResources().getString(R.string.aldente_done);
                text = getResources().getString(R.string.open);
                break;
            case 1:
                ticker = getResources().getString(R.string.optimal_done);
                tittle = getResources().getString(R.string.optimal_done);
                text = getResources().getString(R.string.open);
                break;
        }
        Log.i("NOTIFICATION", ticker + " " + tittle + " " + text);
        Context context = getApplicationContext();

        Intent notificationIntent = new Intent(context, TimerActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(context,
                0, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        //Resources res = context.getResources();
        Notification.Builder builder = new Notification.Builder(context);

        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_alarm_white_24dp)
                // большая картинка
                //.setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.hungrycat))
                //.setTicker(res.getString(R.string.warning)) // текст в строке состояния
                .setTicker(ticker)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                //.setContentTitle(res.getString(R.string.notifytitle)) // Заголовок уведомления
                .setContentTitle(tittle)
                //.setContentText(res.getString(R.string.notifytext))
                .setContentText(text); // Текст уведомления

        //Notification notification = builder.getNotification(); // до API 16
        Notification notification = builder.build();

        notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS |
                Notification.DEFAULT_VIBRATE;
        notification.ledARGB = Color.BLUE;
        notification.ledOffMS = 0;
        notification.ledOnMS = 1;
        notification.flags = notification.flags | Notification.FLAG_SHOW_LIGHTS;

        notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, notification);
    }

    @Override
    public void onAlDenteTimerTick(final int secondsRemaining, float progress) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    @Override
    public void onOptimalTimerTick(final int secondsRemaining, final float progress) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                currentRenderer.updateOptimalPercentage(progress);
                alDenteMinutes.setText(String.valueOf(secondsRemaining / 60));
                if (secondsRemaining % 60 < 10) {
                    alDenteSeconds.setText(String.valueOf("0" + secondsRemaining % 60));
                } else {
                    alDenteSeconds.setText(String.valueOf(secondsRemaining % 60));
                }
            }
        });
    }

    @Override
    public void onUserTimerTick(int secondsRemaining, float progress) {

    }

    @Override
    public void onAlDenteTimerFinished() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isAlDenteFinished) {
                    //alDenteMinutes.setText("00");
                    //alDenteSeconds.setText("00");
                    firstInfoTv.setText("AlDente makaron już ugotowany!");
                    showNotification(0);
                    isAlDenteFinished = true;
                }
            }
        });
    }

    @Override
    public void onOptimalTimerFinished() {
        currentRenderer.updateOptimalPercentage(1.0f); //Ważne dla domknięcia okręgu!
        currentRenderer.setProgressRingDisabled();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alDenteMinutes.setText("00");
                alDenteSeconds.setText("00");
                firstInfoTv.setText("Makaron już ugotowany!");
                startButton.setText("Zapisz czas i zakonć");
                isFinished = true;
                //stopButton.setVisibility(View.VISIBLE);
                enabledElements(ENABLE_MODE_FINISHED);
                //playSoundNotification();
                showNotification(1);
            }
        });
    }

    @Override
    public void onUserTimerFinished() {

    }

    @Override
    public void onElapsedTimeTick(int totalElapsedTime) {
        //notificationManager.cancelAll();

        minutes.setText(String.valueOf(totalElapsedTime / 60));
        if (totalElapsedTime % 60 < 10) {
            seconds.setText(String.valueOf("0" + totalElapsedTime % 60));
        } else {
            seconds.setText(String.valueOf(totalElapsedTime % 60));
        }
        if (totalElapsedTime > boiledTime * 60) {
            afterElapsedTimeNotification++;
            if (afterElapsedTimeNotification >= 30 && afterElapsedTimeNotification % 30 == 0) {
                showNotification(1);
            }
        }

    }

    @Override
    public void onElapsedTimeFinished() {
        noodleTimerFragment.stopAllTimersGetElapsed();
        sendTimeToServer();
        startEndCookingActivity();
    }

    private void startEndCookingActivity() {
        Intent intent = new Intent(TimerActivity.this, EndCookingActivity.class);
        intent.putExtra("time", noodleTimerFragment.getElapsedTime());
        startActivity(intent);
        finish();
    }

    private void sendTimeToServer() {
        showDialog();
        StringRequest postRequest = new StringRequest(Request.Method.POST, URLS.URL_POST_TIME,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.contains("field")) {
                            Toast.makeText(TimerActivity.this, "Error captured while loading data", Toast.LENGTH_SHORT).show();
                        }
                        startEndCookingActivity();
                        hideDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TimerActivity.this, "Error captured while loading data", Toast.LENGTH_SHORT).show();
                        startEndCookingActivity();
                        hideDialog();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap();
                params.put("Time", String.valueOf(noodleTimerFragment.getElapsedTime()));
                params.put("Pasta", String.valueOf(pastaId));
                return params;
            }
        };
        ApplicationController.getInstance().addToRequestQueue(postRequest);
    }

    private void initProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.sending));
        progressDialog.setCancelable(false);
    }

    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }
}

