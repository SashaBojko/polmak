package com.industi.polmak.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.industi.polmak.fragments.BarcodeScanner;
import com.industi.polmak.R;

/*
*   Activity to scan barcode with default camera. Uses BarcodeScanner fragment.
*   Closed when code has been scanned. Returns barcode type and barcode value
*/


public class BarcodeActivity extends AppCompatActivity implements BarcodeScanner.OnFragmentInteractionListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.barcode_activity);

        //define back button over the camera view
        ImageView backArrow = (ImageView) findViewById(R.id.barcode_activity_back_arrow);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //set cancel result and close activity
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    @Override
    public void onCodeScanned(final String eanType, final String eanValue) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //set ok result and return data
                Intent intent = new Intent();
                intent.putExtra("type", eanType);
                intent.putExtra("value", eanValue);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
