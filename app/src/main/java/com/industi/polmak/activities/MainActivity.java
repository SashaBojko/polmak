package com.industi.polmak.activities;

import android.Manifest;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.industi.polmak.BuildConfig;
import com.industi.polmak.R;
import com.industi.polmak.adapters.SearchSuggestionsAdapter;
import com.industi.polmak.controllers.DataMediator;
import com.industi.polmak.enums.FragmentIndex;
import com.industi.polmak.fragments.PastaDetailsFragment;
import com.industi.polmak.fragments.PastaTabFragment;
import com.industi.polmak.fragments.RecipeDetailsFragment;
import com.industi.polmak.fragments.RecipeTabFragment;
import com.industi.polmak.fragments.ScanResultFragment;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "POLMAK_APP";

    //request code to get scanned barcode from onActivityResult
    public static final int REQUEST_CODE_BARCODE = 1;
    public static final int REQUEST_CODE_MAKE_PHOTO = 2;
    public static final int REQUEST_CODE_REQUEST_READ_EXTERNAL_STORAGE = 3;
    public static final int REQUEST_CODE_REQUEST_CAMERA = 4;

    private static FloatingActionButton fab;
    private static ActionBar toolbar;

    //photoFile uri. Uses at share photo to Facebook
    private Uri photoFile;
    private static CallbackManager fbManager;

    private static BottomBar bottomBar;
    private static FragmentManager fragmentManager;
    private PastaTabFragment pastaTabFragment;
    private RecipeTabFragment recipeTabFragment;
    private PastaDetailsFragment pastaDetailsFragment;
    private RecipeDetailsFragment recipeDetailsFragment;
    private ScanResultFragment scanResultFragment;

    private int selectedPastaId = 0;
    private int selectedRecipeId = 0;

    private int previousFragmentId = FragmentIndex.PastaTabFragment;
    private int currentFragmentId = FragmentIndex.PastaTabFragment;
    private boolean ifPastaSelectedLastTime = false;


    private String mCurrentPhotoPath;
    private Uri photoUri;
    private File photo;
    private File storageDir;

    private Fragment currentFragment;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        fbManager = CallbackManager.Factory.create();
        fragmentManager = getSupportFragmentManager();

        LoginManager.getInstance().registerCallback(
                fbManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        AccessToken accessToken = AccessToken.getCurrentAccessToken();
                        if (accessToken.getPermissions().contains("publish_actions")) {
                            //OK
                        } else {
                            handleError();
                        }
                    }

                    @Override
                    public void onCancel() {
                        handleError();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        handleError();
                    }

                    private void handleError() {
                    }
                }
        );

        initFragments();
        initUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //inflate res/menu/main_menu to activity
        getMenuInflater().inflate(R.menu.main_menu, menu);
        //delete action bar elevation
        getSupportActionBar().setElevation(0);

        toolbar = getSupportActionBar();
        //Log.i("TOOLBAR", "INIT");
        initSearchView(menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //back arrow in toolbar
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fbManager.onActivityResult(requestCode, resultCode, data);
        //Log.i("onActivityResult", "result requesr code = " + requestCode + " " + resultCode);
        if (requestCode == REQUEST_CODE_BARCODE) {
            String barcode = "";
            if (resultCode == RESULT_OK) {
                barcode = data.getStringExtra("value");
                int id = DataMediator.getPastaIdByEAN(barcode);
                if (id == -1) {
                    setCurrentFragment(FragmentIndex.ScanResultFragment, 0);
                } else {
                    setCurrentFragment(FragmentIndex.PastaDetailsFragment, id);
                }
            }
            if (resultCode == RESULT_CANCELED) {
                setCurrentFragment(R.id.tab_pastas, 0);
                setTabPosition(R.id.tab_pastas);
            }
        }
        if (requestCode == REQUEST_CODE_MAKE_PHOTO) {
            //Log.i("MAINACTIVITY", "made photo");
            if (resultCode == RESULT_OK) {
                //startFacebookShare();
                shareUsingNativeDialog();
            } else {
                Toast.makeText(this, "Probably you don't have Facebook app or something goes wrong", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        //Log.i("PERMISSION_REQUEST", "sdfs");
        if (requestCode == REQUEST_CODE_REQUEST_READ_EXTERNAL_STORAGE && grantResults.length == 3) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                makePhoto();
            } else {
                Toast.makeText(this, "No permission", Toast.LENGTH_SHORT).show();
            }
        }
        if (requestCode == REQUEST_CODE_REQUEST_CAMERA && grantResults.length == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startActivityForResult(new Intent(MainActivity.this, BarcodeActivity.class), REQUEST_CODE_BARCODE);
            } else {
                Toast.makeText(this, "No permission", Toast.LENGTH_SHORT).show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onBackPressed() {
        Log.i("onBackPressed", "METHOD");
        if (currentFragmentId == FragmentIndex.PastaTabFragment) {
            super.onBackPressed();
            return;
        }
        if (currentFragmentId == FragmentIndex.RecipeDetailsFragment) {
            Log.i("BACK", "RECIPE_DETAILS");
            if (previousFragmentId == FragmentIndex.PastaDetailsFragment) {
                Log.i("BACK", "RECIPE_DETAILS TO PASTAS");
                setCurrentFragment(FragmentIndex.PastaDetailsFragment, selectedPastaId);
                //setTabPosition(R.id.tab_pastas);
            } else {
                Log.i("BACK", "RECIPE_DETAILS TO RECIPES");
                setCurrentFragment(R.id.tab_recipes, 0);
                //setTabPosition(R.id.tab_recipes);
            }
            return;
        }

        if (currentFragmentId == FragmentIndex.RecipeTabFragment || currentFragmentId == FragmentIndex.PastaDetailsFragment || currentFragmentId == FragmentIndex.ScanResultFragment) {
            Log.i("BACK", "PASTAS");
            //setCurrentFragment(FragmentIndex.PastaDetailsFragment, selectedPastaId);
            setTabPosition(R.id.tab_pastas);
        }


    }

    /**
     * Initialization of SearchView in application Toolbar
     *
     * @param menu menu, which contains search item
     */
    private void initSearchView(Menu menu) {
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        final SearchView searchView = (SearchView) menu.findItem(R.id.second_toolbar_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        //set custom adapter
        final SearchSuggestionsAdapter adapter = new SearchSuggestionsAdapter(this);
        searchView.setSuggestionsAdapter(adapter);
        //set suggestion click listener
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionClick(int position) {
                int id = DataMediator.getPastaIdByName(adapter.getListItem(position));
                if (id != -1) {
                    setCurrentFragment(FragmentIndex.PastaDetailsFragment, id);
                } else {
                    id = DataMediator.getRecipeIdByName(adapter.getListItem(position));
                    if (id != -1) {
                        setCurrentFragment(FragmentIndex.RecipeDetailsFragment, id);
                    } else {
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.splash_error_other), Toast.LENGTH_SHORT).show();
                    }
                }
                searchView.clearFocus();
                //searchView.setIconified(true);
                toolbar.collapseActionView();
                return true;
            }

            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Toast.makeText(MainActivity.this, query, Toast.LENGTH_SHORT).show();
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    /**
     * User interface initialization: bottom navigation and floating action button
     */
    private void initUI() {
        initBottomNavigation();
        initFAB();
    }

    private void initFragments() {
        pastaTabFragment = new PastaTabFragment();
        recipeTabFragment = new RecipeTabFragment();
        pastaDetailsFragment = new PastaDetailsFragment();
        recipeDetailsFragment = new RecipeDetailsFragment();
        scanResultFragment = new ScanResultFragment();

        if (fragmentManager.findFragmentByTag(String.valueOf(FragmentIndex.PastaTabFragment)) != null) {
            fragmentManager.beginTransaction().replace(R.id.contentContainer, fragmentManager.findFragmentByTag(String.valueOf(FragmentIndex.PastaTabFragment)), String.valueOf(FragmentIndex.PastaTabFragment)).commit();
        } else {
            fragmentManager.beginTransaction().add(R.id.contentContainer, pastaTabFragment, String.valueOf(FragmentIndex.PastaTabFragment)).commit();
        }
    }

    private void initBottomNavigation() {

        bottomBar = (BottomBar) findViewById(R.id.bottomBar);

        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                setCurrentFragment(tabId, 0);
            }
        });

        bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
            @Override
            public void onTabReSelected(@IdRes int tabId) {
                setCurrentFragment(tabId, 0);
            }
        });
    }

    public void setCurrentFragment(int tabId, int selectedElementId) {
        //Log.i("SET FRAGMENT", "METHOD");
        switch (tabId) {
            case R.id.tab_pastas:
                Log.i("SET_FRAGMENT", "PASTAS");
                if(previousFragmentId == FragmentIndex.PastaDetailsFragment){
                    if (selectedElementId != 0) selectedPastaId = selectedElementId;
                    setPreviousFragmentId(FragmentIndex.PastaDetailsFragment);
                    ifPastaSelectedLastTime = true;
                    PastaDetailsFragment fragment2 = (PastaDetailsFragment) fragmentManager.findFragmentByTag(String.valueOf(FragmentIndex.PastaDetailsFragment));

                    if (fragment2 == null) {
                        pastaDetailsFragment.setPasta(DataMediator.getPastaById(selectedPastaId));
                        fragmentManager.beginTransaction().replace(R.id.contentContainer, pastaDetailsFragment, String.valueOf(FragmentIndex.PastaDetailsFragment)).commit();
                        currentFragment = pastaDetailsFragment;
                    } else {
                        fragment2.setPasta(DataMediator.getPastaById(selectedPastaId));
                        fragmentManager.beginTransaction().hide(currentFragment).show(fragment2).commit();
                        currentFragment = fragment2;
                    }
                    setToolbarSettings(DataMediator.getPastaById(selectedPastaId).getName(), true);
                }else{
                    setPreviousFragmentId(FragmentIndex.PastaTabFragment);
                    PastaTabFragment fragment = (PastaTabFragment) fragmentManager.findFragmentByTag(String.valueOf(FragmentIndex.PastaTabFragment));

                    if (fragment == null) {
                        fragmentManager.beginTransaction().replace(R.id.contentContainer, pastaTabFragment, String.valueOf(FragmentIndex.PastaTabFragment)).commit();
                        currentFragment = pastaTabFragment;
                    } else {
                        fragmentManager.beginTransaction().hide(currentFragment).show(fragment).commit();
                        currentFragment = fragment;
                    }
                    setToolbarSettings(getResources().getString(R.string.bottom_nav_item1), false);
                }
                break;

            case R.id.tab_recipes:
                setPreviousFragmentId(FragmentIndex.RecipeTabFragment);
                RecipeTabFragment fragment1 = (RecipeTabFragment) fragmentManager.findFragmentByTag(String.valueOf(FragmentIndex.RecipeTabFragment));

                if (fragment1 == null) {
                    fragmentManager.beginTransaction().replace(R.id.contentContainer, recipeTabFragment, String.valueOf(FragmentIndex.RecipeTabFragment)).commit();
                    currentFragment = recipeTabFragment;
                } else {
                    fragmentManager.beginTransaction().hide(currentFragment).show(fragment1).commit();
                    //fragmentManager.beginTransaction().replace(R.id.contentContainer, fragment1, String.valueOf(FragmentIndex.RecipeTabFragment)).commit();
                    currentFragment = fragment1;
                }
                setToolbarSettings(getResources().getString(R.string.bottom_nav_item3), false);
                showOrHideFAB(false);
                break;

            case R.id.tab_scan:
                if (fragmentManager.findFragmentByTag(String.valueOf(FragmentIndex.ScanResultFragment)) == null)
                    startBarcodeActivity();
                setToolbarSettings(getResources().getString(R.string.app_name), false);
                showOrHideFAB(false);
                break;

            case FragmentIndex.PastaDetailsFragment:
                Log.i("SET_FRAGMENT", "PASTA_DETAILS");
                if (selectedElementId != 0) selectedPastaId = selectedElementId;
                setPreviousFragmentId(FragmentIndex.PastaDetailsFragment);
                ifPastaSelectedLastTime = true;
                PastaDetailsFragment fragment2 = (PastaDetailsFragment) fragmentManager.findFragmentByTag(String.valueOf(FragmentIndex.PastaDetailsFragment));

                if (fragment2 == null) {
                    pastaDetailsFragment.setPasta(DataMediator.getPastaById(selectedPastaId));
                    fragmentManager.beginTransaction().replace(R.id.contentContainer, pastaDetailsFragment, String.valueOf(FragmentIndex.PastaDetailsFragment)).commit();
                    currentFragment = pastaDetailsFragment;
                } else {
                    fragment2.setPasta(DataMediator.getPastaById(selectedPastaId));
                    fragmentManager.beginTransaction().hide(currentFragment).show(fragment2).commit();
                    //fragmentManager.beginTransaction().replace(R.id.contentContainer, fragment2, String.valueOf(FragmentIndex.PastaDetailsFragment)).commit();
                    currentFragment = fragment2;
                }
                setToolbarSettings(DataMediator.getPastaById(selectedPastaId).getName(), true);
                break;

            case FragmentIndex.RecipeDetailsFragment:
                Log.i("SET_FRAGMENT", "RECIPE_DETAILS");
                if (selectedElementId != 0) selectedRecipeId = selectedElementId;
                setPreviousFragmentId(FragmentIndex.RecipeDetailsFragment);
                RecipeDetailsFragment fragment3 = (RecipeDetailsFragment) fragmentManager.findFragmentByTag(String.valueOf(FragmentIndex.RecipeDetailsFragment));

                if (fragment3 == null) {
                    recipeDetailsFragment.setRecipe(DataMediator.getRecipeById(selectedRecipeId));
                    fragmentManager.beginTransaction().replace(R.id.contentContainer, recipeDetailsFragment, String.valueOf(FragmentIndex.RecipeDetailsFragment)).commit();
                    currentFragment = recipeDetailsFragment;
                } else {
                    fragment3.setRecipe(DataMediator.getRecipeById(selectedRecipeId));
                    fragmentManager.beginTransaction().hide(currentFragment).show(fragment3).commit();
                    //fragmentManager.beginTransaction().replace(R.id.contentContainer, fragment3, String.valueOf(FragmentIndex.RecipeDetailsFragment)).commit();
                    currentFragment = fragment3;
                }
                setToolbarSettings(DataMediator.getRecipeById(selectedRecipeId).getName(), true);
                break;
            case FragmentIndex.ScanResultFragment:
                setPreviousFragmentId(FragmentIndex.ScanResultFragment);
                ScanResultFragment fragment4 = (ScanResultFragment) fragmentManager.findFragmentByTag(String.valueOf(FragmentIndex.ScanResultFragment));
                if (fragment4 == null) {
                    fragmentManager.beginTransaction().replace(R.id.contentContainer, scanResultFragment, String.valueOf(FragmentIndex.ScanResultFragment)).commit();
                    currentFragment = scanResultFragment;
                } else {
                    fragmentManager.beginTransaction().replace(R.id.contentContainer, fragment4, String.valueOf(FragmentIndex.ScanResultFragment)).commit();
                    currentFragment = fragment4;
                }
                setToolbarSettings(getResources().getString(R.string.bottom_nav_item4), false);
                showOrHideFAB(false);
                break;
        }
    }

    private void setPreviousFragmentId(int selectedTabId) {
        if (selectedTabId != currentFragmentId) {
            previousFragmentId = currentFragmentId;
            currentFragmentId = selectedTabId;
        }
        Log.i("IDS", "current = " + currentFragmentId + " previous = " + previousFragmentId);
    }

    public void setTabPosition(int tabPosition) {
        if (tabPosition == R.id.tab_pastas || tabPosition == R.id.tab_recipes) {
            bottomBar.selectTabWithId(tabPosition);
        }
    }

    public void setToolbarSettings(String title, boolean isBackAllow) {
        if (toolbar != null) {
            toolbar.setTitle(title);
            if (isBackAllow) {
                toolbar.setDisplayHomeAsUpEnabled(true);
                toolbar.setDisplayShowHomeEnabled(true);
            } else {
                toolbar.setDisplayHomeAsUpEnabled(false);
                toolbar.setDisplayShowHomeEnabled(false);
            }
        }
    }

    /**
     * Starts activity to read EAN code
     */
    public void startBarcodeActivity() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            startActivityForResult(new Intent(MainActivity.this, BarcodeActivity.class), REQUEST_CODE_BARCODE);
        } else {
            requestCameraPermission();
        }

    }

    //initialization of the floating action button
    private void initFAB() {
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = fragmentManager.findFragmentByTag(String.valueOf(FragmentIndex.PastaDetailsFragment));
                if (fragment != null && fragment instanceof PastaDetailsFragment) {
                    Intent intent = new Intent(MainActivity.this, TimerActivity.class);
                    intent.putExtra("pastaId", selectedPastaId);
                    startActivity(intent);
                } else startActivity(new Intent(MainActivity.this, TimerActivity.class));
            }
        });

    }

    public static void showOrHideFAB(boolean show) {
        if (show) {
            fab.show();
        } else {
            fab.hide();
        }
    }

    /**
     * Starts camera app to make photo
     */
    public void makePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                photoUri = FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePictureIntent, REQUEST_CODE_MAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        photo = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );
        mCurrentPhotoPath = photo.getAbsolutePath();
        return photo;
    }

    private Bitmap getPhoto() {
        Bitmap bitmap = getRotatedImage();
        //((ImageView) findViewById(R.id.image_view)).setImageBitmap(bitmap);
        //galleryAddPic(bitmap);
        return bitmap;
    }

    public Bitmap getRotatedImage() {
        ExifInterface ei;
        Bitmap bitmap = null;
        try {
            ei = new ExifInterface(mCurrentPhotoPath);

            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    Log.i("ROTATE", "90");
                    bitmap = rotateImage(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    Log.i("ROTATE", "180");
                    bitmap = rotateImage(bitmap, 180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    Log.i("ROTATE", "270");
                    bitmap = rotateImage(bitmap, 270);
                    break;
                case ExifInterface.ORIENTATION_NORMAL:
                    Log.i("ROTATE", "NORMAL");
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix,
                true);
    }

    private void galleryAddPic(Bitmap bitmap) {
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, contentUri);
        sendBroadcast(mediaScanIntent);
        /*MediaStore.Images.Media.insertImage(getContentResolver(),
                bitmap,
                "sdfs",
                "sdf");
        /*Intent i=new Intent(Intent.ACTION_VIEW);

        i.setDataAndType(Uri.fromFile(photo), "image/jpeg");
        startActivity(i);*/
    }

    public void requestStoragePermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    },
                    REQUEST_CODE_REQUEST_READ_EXTERNAL_STORAGE);
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA,
                    },
                    REQUEST_CODE_REQUEST_READ_EXTERNAL_STORAGE);
        }
    }

    public void requestCameraPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{
                        Manifest.permission.CAMERA,
                },
                REQUEST_CODE_REQUEST_CAMERA);
    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    public CallbackManager getFbManager() {
        return fbManager;
    }

    public void shareUsingNativeDialog() {
        ShareDialog shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(fbManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Toast.makeText(MainActivity.this, "Wysłałeś zdjęcie!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {
                Toast.makeText(MainActivity.this, "Something went wrong 1", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });

        if (ShareDialog.canShow(SharePhotoContent.class)) {
            SharePhoto photo = new SharePhoto.Builder()
                    .setBitmap(getPhoto())
                    .build();
            SharePhotoContent content = new SharePhotoContent.Builder()
                    .addPhoto(photo)
                    .build();

            shareDialog.show(content);
        }
    }

}
