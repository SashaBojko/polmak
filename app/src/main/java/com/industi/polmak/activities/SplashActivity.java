package com.industi.polmak.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.industi.polmak.R;
import com.industi.polmak.controllers.DataMediator;
import com.industi.polmak.enums.URLS;
import com.industi.polmak.requests.GetRequest;

/**
 * Created by alexb on 09.09.2016.
 */

public class SplashActivity extends AppCompatActivity {
    //error loading data types
    //ERROR_TYPE_INTERNET - if no internet connection
    //ERROR_TYPE_OTHER - loading or other unexpected exception
    private final int ERROR_TYPE_INTERNET = 1;
    private final int ERROR_TYPE_OTHER = 2;

    //error dialog layout - shows when app gets error during loading data or if no internet connection
    private RelativeLayout internetError;
    //text view to show error message in internetError dialog
    private TextView errorTextView;
    //button in internetError dialog for try load again
    private Button updateButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        init();
        start();
    }

    /**
     * Elements initialization
     */
    private void init() {
        updateButton = (Button) findViewById(R.id.splash_update_button);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                start();
            }
        });
        internetError = (RelativeLayout) findViewById(R.id.splash_no_internet);
        errorTextView = (TextView) findViewById(R.id.splash_error_text_view);
    }

    /**
     * Method which start loading data if internet connection is enable
     */
    private void start() {
        if (isInternetConnection()) {
            //hide error dialog if open
            hideError();
            //initialize data in DataMediator
            DataMediator.init(this);
            //start loading
            new LoadData().execute("");
        } else {
            showError(ERROR_TYPE_INTERNET);
        }
    }

    /**
     * ERROR_TYPE_INTERNET - for internet error
     * ERROR_TYPE_OTHER - for other unexpected exception
     *
     * @param type type of error.
     */
    private void showError(int type) {
        internetError.setVisibility(View.VISIBLE);
        if (type == ERROR_TYPE_INTERNET) {
            errorTextView.setText(getResources().getString(R.string.splash_error_internet));
        }
        if (type == ERROR_TYPE_OTHER) {
            errorTextView.setText(getResources().getString(R.string.splash_error_other));
        }
    }

    //hide error dialog
    private void hideError() {
        internetError.setVisibility(View.INVISIBLE);
    }

    /**
     * Check device internet connection
     *
     * @return true - if internet connection
     */
    private boolean isInternetConnection() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    /**
     * AssyncTask to loading data from server. Loading tables Pastas, Recipes, Shapes, Series and Difficulties. If loading fall down - show error dialog.
     */
    private class LoadData extends AsyncTask<String, Void, String> {
        //flag to check if loading finished successful to all tables
        private boolean loadSuccess = false;
        //current time of loading
        private int currentTime = 0;
        //max time of loading
        private int maxWaitingTime = 20000;

        @Override
        protected String doInBackground(String... params) {
            //start loading tables is different threads
            GetRequest pastas = new GetRequest(getApplicationContext(), URLS.URL_GET_PASTAS, GetRequest.GET_PASTAS);
            GetRequest recipes = new GetRequest(getApplicationContext(), URLS.URL_GET_RECIPES, GetRequest.GET_RECIPES);
            GetRequest shapes = new GetRequest(getApplicationContext(), URLS.URL_GET_SHAPES, GetRequest.GET_SHAPES);
            GetRequest series = new GetRequest(getApplicationContext(), URLS.URL_GET_SERIES, GetRequest.GET_SERIES);
            GetRequest recipeCategories = new GetRequest(getApplicationContext(), URLS.URL_GET_RECIPE_CATEGORIES, GetRequest.GET_RECIPE_CATEGORIES);

            try {
                while (true) {
                    //check if loadFinished every 0,5 second
                    if ((!(pastas.isError() || pastas.isSuccess())) ||
                            (!(recipes.isSuccess() || recipes.isError())) ||
                            (!(shapes.isSuccess() || shapes.isError())) ||
                            (!(series.isSuccess() || series.isError())) ||
                            (!(recipeCategories.isSuccess() || recipeCategories.isError()))) {
                        //Log.i("STATES", " = " + pastas.isError() + " " + pastas.isSuccess()  + " " +  recipes.isSuccess() + " " + recipes.isError() + " " +  shapes.isSuccess() + " " + shapes.isError() + " " +  series.isSuccess() + " " + series.isError() + " " +  recipeCategories.isSuccess() + " " + recipeCategories.isError());

                        //loading is in process
                        //if current time is bigger than max time - end of loading
                        if (currentTime > maxWaitingTime) return "wrong";
                        //increment current time and stop main thread for  0,5 seconds
                        currentTime += 500;
                        Thread.sleep(500);
                    } else {
                        //Log.i("STATES", "pastas = " + pastas.isSuccess() + " recipes = " + recipes.isSuccess() + " shapes = " + shapes.isSuccess() + " series = " + series.isSuccess() + " recipesCategories = " + recipeCategories.isSuccess() + " recipesCategoriesAll = " + (!(recipeCategories.isSuccess() || recipeCategories.isError())));

                        //if all tables loaded successfully
                        if (pastas.isSuccess() && recipes.isSuccess() && shapes.isSuccess() && series.isSuccess() && recipeCategories.isSuccess()) {
                            //loaded successful
                            loadSuccess = true;
                        }
                        return "loadFinished";
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "wrong";
        }

        @Override
        protected void onPostExecute(String result) {

            if (result.equals("loadFinished")) {
                if (loadSuccess) {
                    //everything is ok - start MainActivity
                    hideError();
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                } else {
                    //something wrong - show error
                    showError(ERROR_TYPE_OTHER);
                }
            }
            if (result.equals("wrong")) {
                showError(ERROR_TYPE_OTHER);
            }
        }
    }
}

