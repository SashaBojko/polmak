package com.industi.polmak.adapters;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.industi.polmak.controllers.DataMediator;
import com.industi.polmak.fragments.PastaFragment;

public class PastaViewPagerAdapter extends FragmentPagerAdapter {

    public PastaViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {
        return new PastaFragment().newInstance(position);
    }

    @Override
    public int getCount() {
        return DataMediator.getSeries().size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return DataMediator.getSeries().get(position).getName();
    }
}

