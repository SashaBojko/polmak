package com.industi.polmak.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.industi.polmak.R;
import com.industi.polmak.basic.Ingredient;
import com.industi.polmak.basic.Recipe;
import com.industi.polmak.db.DataBaseHelper;

import java.util.ArrayList;

/**
 * Created by Саша on 05.09.2016.
 */

public class RecyclerViewIngredientsAdapter extends RecyclerView.Adapter<RecyclerViewIngredientsAdapter.MyViewHolder> {
    private ArrayList<Ingredient> ingredients;
    private ArrayList<Integer> ingredientsListIds;
    private int recipeId;

    private DataBaseHelper db;
    private Context context;

    public RecyclerViewIngredientsAdapter(Context context, Recipe recipe, ArrayList list) {
        this.context = context;
        db = new DataBaseHelper(context);
        recipeId = recipe.getId();
        ingredients = list;
        ingredientsListIds = new ArrayList<>();

        String s1 = db.getIngredientsById(recipeId);
        if (!s1.isEmpty()) {
            String[] s = s1.split("\\s+");
            for (String value : s) {
                Log.i("INGREDIENT", "position " + value);
                ingredientsListIds.add(Integer.valueOf(value));
            }
        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ingredient_rv_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.name.setText(ingredients.get(position).getName());
        if(ingredientsListIds.contains((Integer)position)){
            setChecked(holder);
        }
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeAnimation(holder,position);
            }
        });
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeAnimation(holder, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return ingredients.size();
    }

    private void makeAnimation(final MyViewHolder holder, final int position) {
        Animation flipAnim = AnimationUtils.loadAnimation(context, R.anim.flip_anim);
        holder.checkBox.startAnimation(flipAnim);
        flipAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.i("POSITION", position + "");
                if (!ingredientsListIds.contains(position)) {
                    setChecked(holder);
                    ingredientsListIds.add(position);
                } else {
                    setUnchecked(holder);
                    ingredientsListIds.remove((Integer)position);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    private void setChecked(final MyViewHolder holder){
        //holder.name.setBackgroundColor(context.getResources().getColor(R.color.light_green));
        holder.checkBox.setImageResource(R.mipmap.ic_check_box_white_24dp);
        holder.checkBox.setColorFilter(ContextCompat.getColor(context,R.color.green));
        holder.name.setTextColor(context.getResources().getColor(R.color.green));
    }

    private void setUnchecked(final MyViewHolder holder){
        //holder.name.setBackgroundColor(context.getResources().getColor(R.color.textColorWhite));
        holder.checkBox.setImageResource(R.mipmap.ic_check_box_outline_blank_black_24dp);
        holder.checkBox.setColorFilter(ContextCompat.getColor(context,R.color.textColorPrimary));
        holder.name.setTextColor(context.getResources().getColor(R.color.textColorPrimary));
    }

    public void saveSelectedIngredients(){
        Log.i("SAVE", "INGREDIENTS");
        String s = "";
        for(int i=0;i<ingredientsListIds.size();i++){
            if(i== ingredientsListIds.size()-1){
                s = String.valueOf(s + ingredientsListIds.get(i));
            }else{
                s = String.valueOf(s + ingredientsListIds.get(i) + " ");
            }
        }
        db.addIngredients(recipeId, s);
        db.close();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView checkBox;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.ingredient_item_name);
            checkBox = (ImageView) itemView.findViewById(R.id.ingredient_item_check_box);
        }
    }
}

