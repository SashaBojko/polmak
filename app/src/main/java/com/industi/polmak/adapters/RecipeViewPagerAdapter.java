package com.industi.polmak.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.industi.polmak.controllers.DataMediator;
import com.industi.polmak.fragments.RecipeFragment;

/**
 * Created by alexb on 01.09.2016.
 */

public class RecipeViewPagerAdapter extends FragmentPagerAdapter {

    public RecipeViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return RecipeFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return DataMediator.getRecipeCategories().size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return DataMediator.getRecipeCategories().get(position).getName();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        ((RecipeFragment) object).refresh();
        super.setPrimaryItem(container, position, object);
    }
}

