package com.industi.polmak.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.industi.polmak.R;
import com.industi.polmak.activities.MainActivity;
import com.industi.polmak.basic.Recipe;
import com.industi.polmak.db.DataBaseHelper;
import com.industi.polmak.enums.FragmentIndex;
import com.industi.polmak.requests.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by alexb on 01.09.2016.
 */

public class RecyclerViewRecipeAdapter extends RecyclerView.Adapter<RecyclerViewRecipeAdapter.MyViewHolder> {
    private ArrayList<Recipe> recipes;
    private DataBaseHelper db;
    private ImageLoader imageLoader;
    private Activity activity;

    public RecyclerViewRecipeAdapter(Activity activity, ArrayList list) {
        this.activity = activity;
        recipes = list;
        db = new DataBaseHelper(activity);
        imageLoader = new ImageLoader(activity);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_recipe, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Recipe recipe = recipes.get(position);

        imageLoader.setImage(holder.image, recipe.getImageLink());
        //Picasso.with(holder.image.getContext()).load(Uri.parse(recipe.getImageLink())).into(holder.image);
        holder.name.setText(recipe.getName());
        holder.time.setText(getHoursFromMinutes(recipe.getTime()));
        holder.calories.setText(String.valueOf(recipe.getEnergyValue() + " kcal"));
        holder.price.setText(String.valueOf(recipe.getPrice() + " zł/os"));
        holder.level.setText(getLevelName(recipe.getDifficulty()));
        holder.description.setText(recipe.getDescription());
        holder.seeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)activity).setCurrentFragment(FragmentIndex.RecipeDetailsFragment, recipe.getId());
            }
        });
        if(db.isRecipeLiked(recipe.getId())){
            holder.likeImage.setImageResource(R.mipmap.ic_heart);
            holder.likeImage.setColorFilter(Color.RED);
        }

        holder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 if(db.isRecipeLiked(recipe.getId())){
                    db.deleteLike(recipe.getId());
                    holder.likeImage.setImageResource(R.mipmap.ic_heart_outline);
                    holder.likeImage.clearColorFilter();
                }else{
                    db.addLike(recipe.getId());
                    holder.likeImage.setImageResource(R.mipmap.ic_heart);
                    holder.likeImage.setColorFilter(Color.RED);
                }

                //Toast.makeText(context, "Like item id = " + recipe.getId(), Toast.LENGTH_SHORT).show();
            }
        });
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)activity).setCurrentFragment(FragmentIndex.RecipeDetailsFragment, recipe.getId());
            }
        });
        /*holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(context, "Share item id = " + recipe.getId(), Toast.LENGTH_SHORT).show();

            }
        });*/
    }

    private String getLevelName(int level) {
        switch (level) {
            case 1:
                return "Łatwe";
            case 2:
                return "Średnie";
            default:
                return "Trudne";
        }
    }

    private String getHoursFromMinutes(int minutes) {
        if (minutes < 60) {
            return minutes + " min";
        } else {
            int hours = minutes / 60;
            int m = minutes % 60;
            if (m != 0) {
                return hours + " godz " + m + " min";
            } else {
                return hours + " godz";
            }
        }
    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image, likeImage;
        TextView name, time, calories, price, level, description;
        Button seeMore;
        LinearLayout like, share;
        LinearLayout cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            cardView = (LinearLayout)itemView.findViewById(R.id.card_view_recipe);
            image = (ImageView) itemView.findViewById(R.id.card_view_recipe_image);
            name = (TextView) itemView.findViewById(R.id.card_view_recipe_name);
            time = (TextView) itemView.findViewById(R.id.card_view_recipe_time);
            calories = (TextView) itemView.findViewById(R.id.card_view_recipe_calories);
            price = (TextView) itemView.findViewById(R.id.card_view_recipe_price);
            level = (TextView) itemView.findViewById(R.id.card_view_recipe_level);
            description = (TextView) itemView.findViewById(R.id.card_view_recipe_description);
            seeMore = (Button) itemView.findViewById(R.id.card_view_recipe_see_more_button);
            like = (LinearLayout) itemView.findViewById(R.id.card_view_recipe_like);
            likeImage = (ImageView)itemView.findViewById(R.id.card_view_recipe_like_image);
            //share = (LinearLayout) itemView.findViewById(R.id.card_view_recipe_share);
        }
    }
}

