package com.industi.polmak.adapters;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.industi.polmak.R;
import com.industi.polmak.activities.MainActivity;
import com.industi.polmak.basic.Pasta;
import com.industi.polmak.enums.FragmentIndex;
import com.industi.polmak.requests.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by alexb on 26.08.2016.
 */

public class RecyclerViewPastaAdapter extends RecyclerView.Adapter<RecyclerViewPastaAdapter.MyViewHolder> {
    private ArrayList<Pasta> pastas;
    private Activity activityContext;
    private ViewGroup viewGroup;
    //private ContextWrapper cw;
    //private File directory, imageFile;
    private ImageLoader imageLoader;

    public RecyclerViewPastaAdapter(Activity context, ArrayList list) {
        pastas = list;
        activityContext = context;
        imageLoader = new ImageLoader(context);

        //cw = new ContextWrapper(getApplicationContext());
        //directory = cw.getDir("images", Context.MODE_PRIVATE);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.viewGroup = parent;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_pasta, parent, false);

        //get screen size
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activityContext.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        //set pasta card size ( 1/2 of width and 2/4 of height)
        v.setLayoutParams(new CardView.LayoutParams(
                width / 2, (int) (height / 2.4)));
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Pasta pasta = pastas.get(position);
        holder.name.setText(pasta.getName());
        holder.frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)activityContext).setCurrentFragment(FragmentIndex.PastaDetailsFragment, pasta.getId());
            }
        });

        //Picasso.with(holder.image.getContext()).load(Uri.parse(pasta.getImageLink())).into(ImageLoader.getImage(activityContext.getApplicationContext(), "images", pasta.getImageLink().hashCode() + ".png"));

        imageLoader.setImage(holder.image, pasta.getImageLink());
        /*imageFile = new File(directory, pasta.getImageLink().hashCode() + ".png");
        if (!imageFile.exists()) {
            Log.i("IMAGE", "NOT EXISTS");
            Picasso.with(holder.image.getContext()).load(Uri.parse(pasta.getImageLink())).into(ImageLoader.getImage(activityContext.getApplicationContext(), pasta.getImageLink().hashCode() + ".png"));
            Picasso.with(holder.image.getContext()).load(Uri.parse(pasta.getImageLink())).into(holder.image);
        }else{
            Picasso.with(holder.image.getContext()).load(imageFile).into(holder.image);
        }*/
        //Picasso.with(holder.image.getContext()).load(Uri.parse(pasta.getImageLink())).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return pastas.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView image;
        FrameLayout frameLayout;

        MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.card_view_makaron_name);
            image = (ImageView) itemView.findViewById(R.id.card_view_makaron_image);
            frameLayout = (FrameLayout) itemView.findViewById(R.id.card_view_makaron_click_layout);
        }
    }
}
