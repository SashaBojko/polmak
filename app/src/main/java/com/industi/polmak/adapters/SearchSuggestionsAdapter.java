package com.industi.polmak.adapters;

import android.content.Context;
import android.database.AbstractCursor;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;

import com.industi.polmak.R;
import com.industi.polmak.basic.Pasta;
import com.industi.polmak.basic.Recipe;
import com.industi.polmak.controllers.DataMediator;

import java.util.ArrayList;

/**
 * Created by alexb on 01.09.2016.
 */
public class SearchSuggestionsAdapter extends SimpleCursorAdapter {
    private static final String[] mFields = {"_id", "result"};
    private static final String[] mVisible = {"result"};
    private static final int[] mViewIds = {android.R.id.text1};
    private static ArrayList<String> mResults;

    public SearchSuggestionsAdapter(Context context) {
        super(context, R.layout.search_suggestion_row, null, mVisible, mViewIds, 0);
    }

    @Override
    public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
        return new SuggestionsCursor(constraint);
    }

    private static class SuggestionsCursor extends AbstractCursor {

        public SuggestionsCursor(CharSequence constraint) {
            mResults = getSuggestionsList(constraint.toString());
        }

        private ArrayList<String> getSuggestionsList(String string){
            string = string.toLowerCase();
            ArrayList<String> startWith = new ArrayList<>();
            ArrayList<String> contains = new ArrayList<>();
            for(Pasta pasta : DataMediator.getPastas()){
                if(pasta.getName().toLowerCase().startsWith(string)){
                    startWith.add(pasta.getName());
                }
                if(pasta.getName().toLowerCase().contains(string) && !startWith.contains(pasta.getName())){
                    contains.add(pasta.getName());
                }
            }
            for(Recipe recipe : DataMediator.getRecipes()){
                if(recipe.getName().toLowerCase().startsWith(string)){
                    startWith.add(recipe.getName());
                }
                if(recipe.getName().toLowerCase().contains(string) && !startWith.contains(recipe.getName())){
                    contains.add(recipe.getName());
                }
            }
            startWith.addAll(contains);
            return startWith;
        }
        @Override
        public int getCount() {
            return mResults.size();
        }

        @Override
        public String[] getColumnNames() {
            return mFields;
        }

        @Override
        public long getLong(int column) {
            if (column == 0) {
                return mPos;
            }
            throw new UnsupportedOperationException("unimplemented");
        }
        @Override
        public String getString(int column) {
            if (column == 1) {
                return mResults.get(mPos);
            }
            throw new UnsupportedOperationException("unimplemented");
        }

        @Override
        public short getShort(int column) {
            throw new UnsupportedOperationException("unimplemented");
        }

        @Override
        public int getInt(int column) {
            throw new UnsupportedOperationException("unimplemented");
        }

        @Override
        public float getFloat(int column) {
            throw new UnsupportedOperationException("unimplemented");
        }

        @Override
        public double getDouble(int column) {
            throw new UnsupportedOperationException("unimplemented");
        }

        @Override
        public boolean isNull(int column) {
            return false;
        }
    }
    public String getListItem(int position){
        return mResults.get(position);
    }
}

