package com.industi.polmak.controllers;

import android.content.Context;
import android.support.annotation.Nullable;

import com.industi.polmak.basic.Pasta;
import com.industi.polmak.basic.Recipe;
import com.industi.polmak.basic.RecipeCategory;
import com.industi.polmak.basic.Series;
import com.industi.polmak.basic.Shape;
import com.industi.polmak.db.DataBaseHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by alexb on 09.09.2016.
 */

public class DataMediator {
    private static ArrayList<Pasta> pastas;
    private static ArrayList<Recipe> recipes;
    private static ArrayList<Shape> shapes;
    private static ArrayList<Series> series;
    private static ArrayList<RecipeCategory> recipeCategories;
    private static Context context;

    public static void init(Context context1) {
        context = context1;
        pastas = new ArrayList<>();
        recipes = new ArrayList<>();
        shapes = new ArrayList<>();
        series = new ArrayList<>();
        recipeCategories = new ArrayList<>();
    }

    public static void addPasta(Pasta pasta) {
        if (pastas == null) pastas = new ArrayList<>();
        pastas.add(pasta);
    }

    public static ArrayList<Pasta> getPastas() {
        return pastas;
    }

    public static void addRecipe(Recipe recipe) {
        if (recipes == null) recipes = new ArrayList<>();
        recipes.add(recipe);
    }

    public static ArrayList<Recipe> getRecipes() {
        return recipes;
    }

    public static void addShape(Shape shape) {
        if (shapes == null) shapes = new ArrayList<>();
        shapes.add(shape);
    }

    public static ArrayList<Series> getSeries() {
        return series;
    }

    public static void addSeries(Series s) {
        series.add(s);
    }

    @Nullable
    public static Shape getShapeById(int id) {
        for (Shape shape : shapes) {
            if (shape.getId() == id) {
                return shape;
            }
        }
        return null;
    }

    public static void addRecipeCategory(RecipeCategory category) {
        if (recipeCategories == null) recipeCategories = new ArrayList<>();
        recipeCategories.add(category);
    }

    public static ArrayList<RecipeCategory> getRecipeCategories() {
        return recipeCategories;
    }

    @Nullable
    public static Pasta getPastaById(int id) {
        for (Pasta pasta : pastas) {
            if (pasta.getId() == id) {
                return pasta;
            }
        }
        return null;
    }

    @Nullable
    public static Recipe getRecipeById(int id) {
        for (Recipe recipe : recipes) {
            if (recipe.getId() == id) {
                return recipe;
            }
        }
        return null;
    }

    public static ArrayList<Recipe> getRecipesByPastaId(int pastaId) {
        ArrayList<Recipe> list = new ArrayList<>();
        for (Recipe recipe : recipes) {
            //Log.i("DATAMEDITOR", "ID = " + recipe.getPastaId() + " " + pastaId);
            if (recipe.getPastaId() == pastaId) {
                //Log.i("DATAMEDITOR", "ADD RECIPE");
                list.add(recipe);
            }
        }
        return list;
    }


    public static ArrayList<Pasta> getPastasBySeriesId(int id) {
        ArrayList<Pasta> list = new ArrayList<>();
        for (Pasta pasta : pastas) {
            if (pasta.getSeries() == id) {
                list.add(pasta);
            }
        }
        return list;
    }

    private static boolean ifRecipeCategoryIsFavorite(int category) {
        for (RecipeCategory c : recipeCategories) {
            if (c.getId() == category) {
                if (c.getName().equals("Ulubione")) {
                    return true;
                }
            }
        }
        return false;
    }

    public static ArrayList<Recipe> getRecipesByCategoryId(int categoryId) {
        if (ifRecipeCategoryIsFavorite(categoryId)) {
            return getFavoritesRecipes();
        }
        ArrayList<Recipe> list = new ArrayList<>();
        for (Recipe recipe : recipes) {
            //Log.i("DATAMEDITOR", "ID = " + recipe.getPastaId() + " " + pastaId);
            if (recipe.getPastaCategoryId() == categoryId) {
                //Log.i("DATAMEDITOR", "ADD RECIPE");
                list.add(recipe);
            }
        }
        return list;
    }

    private static ArrayList<Recipe> getFavoritesRecipes() {
        DataBaseHelper db = new DataBaseHelper(context);
        ArrayList<Recipe> recipeList = new ArrayList<>();
        ArrayList<Integer> favoriteRecipesIds = db.getLikes();
        for (int id : favoriteRecipesIds) {
            //Log.i("DATAMEDITOR", "ID = " + recipe.getPastaId() + " " + pastaId);
            recipeList.add(getRecipeById(id));
        }
        return recipeList;
    }

    public static int getPastaIdByEAN(String ean){
        for(Pasta pasta : pastas){
            if(pasta.getEanCode().equals(ean)){
                return pasta.getId();
            }
        }
        return -1;
    }

    public static int getPastaIdByName(String name){
        for(Pasta pasta : pastas){
            if(pasta.getName().equals(name)){
                return pasta.getId();
            }
        }
        return -1;
    }

    public static int getRecipeIdByName(String name){
        for(Recipe recipe : recipes){
            if(recipe.getName().equals(name)){
                return recipe.getId();
            }
        }
        return -1;
    }

    /*public static void printPastas() {
        for (Pasta pasta : pastas) {
            Log.i("PASTA + " + pasta.getId(), pasta.toString());
        }
    }

    public static void printRecipes() {
        for (Recipe recipe : recipes) {
            Log.i("RECIPE + " + recipe.getId(), recipe.toString());
        }
    }*/
}
