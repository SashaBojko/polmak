package com.industi.polmak.fragments;

import android.content.Context;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.images.Size;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.industi.polmak.R;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * Fragment obsługujący skanowanie kodów kreskowych EAN8 i EAN13 (ustawienie na sztywno w kodzie)
 * oraz wyświetlanie podglądu z kamery urządzenia. Nie implementuje nakładki obramowującej kod
 * z uwagi na niestabilność danych uzyskiwanych z detektora wbudowanego w "Usługi Google Play".
 */
public class BarcodeScanner extends Fragment {
    //Znacznik do wpisów w LOGu (na potrzeby dodatkowego zgłaszania błędów)
    private final String LOG_ENTRY_TAG = "BarcodeScanner";

    //Nasłuch zdarzeń we fragmencie, do zwracania wyników
    private OnFragmentInteractionListener barcodeScannedEventListener;

    //Powierzchnia rysowania podglądu wideo oraz kamera z której uzyskujemy obraz
    private SurfaceView cameraPreviewSurface;
    private CameraSource previewCameraSource;
    private boolean cameraPerviewIsReady = false;

    public BarcodeScanner() {
        //Wymagany pusty konstruktor publiczny dla fragmentu
    }

    //"Fabryka" skanerów kodów
    @SuppressWarnings("unused")
    public static BarcodeScanner newInstance() {
        return new BarcodeScanner();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_barcode_scanner, container, false);

        //Podłączenie powierzchni podglądu dla kamery
        cameraPreviewSurface = (SurfaceView) fragmentView.findViewById(R.id.cameraPreviewSurface);
        cameraPreviewSurface.getHolder().addCallback(new PreviewSurfaceCallback());
        cameraPerviewIsReady = true; //Powierzchnia dla podglądu kamery jest już gotowa

        fragmentView.setKeepScreenOn(true); //Podczas skanowania ekran ma nie gasnąć

        return fragmentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            barcodeScannedEventListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " musi zaimplementować OnFragmentInteractionListener (zwrot wyniku)!");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        barcodeScannedEventListener = null;
        if (previewCameraSource != null) {releasePreview();}
    }

    //Uruchamianie i zatrzymywanie podglądu oraz wykrywania (na potrzeby zmiany stanu)
    public void startPreview() {
        if (previewCameraSource == null) {
            stopPreview();
        } else {
            try {
                startIfReady();
            } catch (IOException e) {
                //Prosta obsługa wyjątku we/wy - potrzebna lepsza implementacja?
                Log.e(LOG_ENTRY_TAG, "Nie udało się uruchomić podglądu: ", e);
                //Wyświetlenie prostego komunikatu dla użytkownika
                Toast.makeText(getContext(),"Błąd uruchamiania podglądu skanera kodów.",Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void stopPreview() {
        if (previewCameraSource != null) {
            previewCameraSource.stop();
        }
    }

    public void releasePreview() {
        if (previewCameraSource != null) {
            previewCameraSource.release();
            previewCameraSource = null;
        }
    }

    /**
     * Przeciążenia trzech poniższych metod zapobiegają problemom z podglądem z kamery podczas zawieszania
     * i niszczenia fragmentu razem z nadrzędną aplikacją.
     */

    @Override
    public void onResume() {
        super.onResume();
        startPreview();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (previewCameraSource != null) {
            stopPreview();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (previewCameraSource != null) {
            releasePreview();
        }
    }

    /**
     * Interfejs służący do zwracania wyników z czytnika kodów. Musi zostać zaimplementowany przez
     * aktywność wykorzystującą fragment.
     */
    public interface OnFragmentInteractionListener {
        /**
         * Metoda wywoływana jest w momencie, gdy zostanie ustalony poprawny wynik skanowania kodów EAN
         * (po głosowaniu na trzy wyniki przez tracker). Implementująca klasa musi ten wynik obsłużyć
         * we własnym zakresie.
         *
         * @param eanType   String  typ kodu EAN wykrytego przez skaner (EAN8 lub EAN13)
         * @param eanValue  String  wartość RAW kodu odczytana przez skaner (wybrany wynik z trzech próbek)
         */
        void onCodeScanned(String eanType, String eanValue);
    }

    /*
     * Metody konfigurujące maksymalną rozdzielczość podglądu oraz częstotliwość klatek na sekundę
     * dla kamery zwróconej do tyłu. Używanie przedniej kamery do czytania kodów raczej nie ma zbyt
     * wiele sensu. Kamera w obu przypadkach otwierana jest tylko na chwilę.
     */

    /**
     * Wybór maksymalnej częstotliwości klatek dla podglądu z kamery. Z uwagi na konieczność zachowania
     * zgodności ze starszym API, wyciszone ostrzeżenia o przestarzałych metodach.
     *
     * @return  int ilość klatek na sekundę
     */
    @SuppressWarnings("deprecation")
    private int findMaximumCameraPreviewFramerate() {
        //Pobranie informacji z kamery
        Camera previewCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
        List<Integer> supportedPreviewFramerates = previewCamera.getParameters().getSupportedPreviewFrameRates();
        previewCamera.release(); //Zamknięcie kamery
        //Wyszukanie maksymalnego FPS dla podglądu
        int foundPreviewFPS = 0;
        Iterator previewFramerateIterator = supportedPreviewFramerates.iterator();
        while(previewFramerateIterator.hasNext()) {
            int currentPrvFPS = (int) previewFramerateIterator.next();
            if(currentPrvFPS > foundPreviewFPS) {foundPreviewFPS = currentPrvFPS;}
        }
        previewFramerateIterator.remove();
        supportedPreviewFramerates.clear();
        //Zwrot częstotliwości klatek podglądu
        return foundPreviewFPS;
    }

    /**
     * Wybór maksymalnej dostępnej rozdzielczości dla podlgądu z kamery (również dla algorytmu detekcji).
     * Z uwagi na konieczność zachowania zgodności ze starszym API, wyciszone ostrzeżenia o przestarzałych
     * metodach.
     *
     * @return  Size    rozmiary podglądu/bitmap dostarczanych dla tetektora (szerokość,wysokość)
     */
    @SuppressWarnings("deprecation")
    private Size findMaximumCameraPreviewSize(){
        Camera previewCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
        List<Camera.Size> supportedPreviewSizes = previewCamera.getParameters().getSupportedPreviewSizes();
        previewCamera.release(); //Zamknięcie kamery
        //Wyszukanie maksymalnego dostępnego rozmiaru podglądu
        Iterator previewSizeIterator = supportedPreviewSizes.iterator();
        int maxPreviewW = 0, maxPreviewH = 0;
        while(previewSizeIterator.hasNext()) {
            Camera.Size currentSize = (Camera.Size) previewSizeIterator.next();
            //Jeśli oba wymiary są większe od poprzednich, zastępujemy
            if((currentSize.width > maxPreviewW) && (currentSize.height > maxPreviewH)) {
                maxPreviewW = currentSize.width; maxPreviewH = currentSize.height;
            }
        }
        previewSizeIterator.remove();
        supportedPreviewSizes.clear();
        //Zwrot wymiarów bufora ramki
        return new Size(maxPreviewW, maxPreviewH);
    }

    /**
     * Callback dla powierzchni podglądu kamery. Potrzebny przy tworzeniu, usuwaniu i zmianach właściwości
     * docelowego SurfaceView. Obsługa wyjątków jest ograniczona do wyświetlania komunikatów
     * i przerwania działania fragmentu - być może potrzebna jest lepsza implementacja?
     */
    private class PreviewSurfaceCallback implements SurfaceHolder.Callback {
        @Override
        public void surfaceCreated(SurfaceHolder surface) {
            try {
                //Jeśli jesteśmy gotowi, możemy startować
                startIfReady();
                //Prosta obsługa wyjątków (z komunikatami) - potrzebna lepsza?
            } catch (SecurityException secEx) {
                Log.e(LOG_ENTRY_TAG, "Brak uprawnień do uruchomienia kamery.",secEx.getCause());
                //Wyświetlenie prostego komunikatu dla użytkownika
                Toast.makeText(getContext(),"Brak uprawnień do uruchomienia kamery",Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                Log.e(LOG_ENTRY_TAG, "Nie udało się uruchomić podglądu: ", e);
                //Wyświetlenie prostego komunikatu dla użytkownika
                Toast.makeText(getContext(),"Błąd uruchamiania podglądu skanera kodów.",Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder surface) {
            //Jeśli powierzchnia zostanie zniszczona, trzeba natychmiast zatrzymać kamerę
            releasePreview();
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            //Pusta metoda (nie trzeba nic robić w przypadku zmian w powierzchni)
        }
    }

    /**
     * Metoda uruchamiająca kamerę jeśli powierzchnia jest gotowa. Ustawia parametry pracy dla podglądu,
     * tworzy skaner i detektor kodów kreskowych. Zawsze wykorzystuje autofokus.
     */

    //Uruchomienie podglądu z kamery
    private void startIfReady() throws IOException, SecurityException {
        if (cameraPerviewIsReady) {
            //Ustawienie parametrów kamery
            Size cameraFrameBufferSize = findMaximumCameraPreviewSize();
            int cameraPreviewFramerate = findMaximumCameraPreviewFramerate();

            //Utworzenie nowego skanera kodów EAN
            EANBarcodeScanner eanBarcodeScanner = new EANBarcodeScanner();
            BarcodeDetector eanBarcodeDetector = eanBarcodeScanner.getEanBarcodeDetector();

            //Zbudowanie źródła obrazu
            CameraSource.Builder cameraSourceBuilder = new CameraSource.Builder(getContext(),eanBarcodeDetector)
                    .setFacing(CameraSource.CAMERA_FACING_BACK) //Zawsze tylna kamera
                    .setRequestedPreviewSize(cameraFrameBufferSize.getWidth(),cameraFrameBufferSize.getHeight())
                    .setRequestedFps(cameraPreviewFramerate)
                    .setAutoFocusEnabled(true); //Minimum API 17, więc autofokus jest dostepny
            previewCameraSource = cameraSourceBuilder.build();

            //Uruchomienie kamery z ustawionymi opcjami
            previewCameraSource.start(cameraPreviewSurface.getHolder());
        }
    }

    /**
     *
     * Klasa pozwala na skonstruowanie skanera kodów EAN do wykorzystania we współpracy z CameraPreview.
     * Prywatne podklasy realizują niezbędne do wykrywania funkcje (nie są potrzebne nigdzie indziej)
     *
     */
    class EANBarcodeScanner {
        //Pola
        BarcodeDetector eanBarcodeDetector;
        EANBarcodeTrackerFactory eanBarcodeTrackerFactory;

        //Konstruktor
        public EANBarcodeScanner() {
            eanBarcodeDetector = new BarcodeDetector.Builder(getContext())
                    .setBarcodeFormats(Barcode.EAN_8 | Barcode.EAN_13) //Ograniczenie typów rozpoznawanych kodów (wydajność)
                    .build();

            eanBarcodeTrackerFactory = new EANBarcodeTrackerFactory();
            //Przypisanie procesora do detektora kodów
            eanBarcodeDetector.setProcessor(new MultiProcessor.Builder<>(eanBarcodeTrackerFactory).build());
        }

        //Dostęp
        public BarcodeDetector getEanBarcodeDetector() {return eanBarcodeDetector;}
    }

    /**
     * Fabryka wytwarzająca trackery dla każdego wykrytego kodu kreskowego. Zwykle powstanie tylko jeden,
     * po czym od razu zostanie zwrócony wynik skanowania kodu kreskowego.
     */

    class EANBarcodeTrackerFactory implements MultiProcessor.Factory<Barcode> {
        //Stosuje pusty konstruktor domyślny (nie ma potrzeby deklaracji)

        @Override
        public Tracker<Barcode> create(Barcode barcode) {
            return new EANBarcodeTracker();
        }
    }

    /**
     * Tracker do śledzenia kodów kreskowych. Wywołuje zwrot wyniku w przypadku odczytania kodu
     * w znanym formacie (EAN8 lub EAN13) poprzez aktywność nadrzędną. Jako wynik podawany jest głos z
     * trzech wyników próbkowanych w trzech kolejnych wykryciach (dla redukcji ilości błędów)
     */

    class EANBarcodeTracker extends Tracker<Barcode> {
        //Pola służące do głosowania nad wynikami
        private int typeDetections[] = new int[3];
        private String rawDetections[] = new String[3];
        private int successfulDetectionCount = 0;

        //Stosuje pusty konstruktor domyślny (nie ma potrzeby deklaracji)

        //Wywoływane zaraz na początku śledzenia
        @Override
        public void onNewItem(int id, Barcode item) {
            //Tutaj nie ma potrzeby nic robić
        }

        //Aktualizacja potwierdza obecność kodu, tutaj zostanie on odczytany
        @Override
        public void onUpdate(Detector.Detections<Barcode> detectionResults, Barcode item) {
            if(successfulDetectionCount>2) {
                /**
                 * Głosowanie nad trzema kolejnymi wynikami wykrywania kodów. Konieczne z uwagi na
                 * pojawiające się podczas testów przekłamania w wykrywaniu. Wybiera wynik który
                 * wystąpił w dwóch na trzy przypadkach, a jeśli wszystkie trzy są różne, wybiera
                 * ostatni.
                 */
                int tempFormat; String tempRawValue;
                if(typeDetections[0]==typeDetections[1] && rawDetections[0].equals(rawDetections[1])){
                    tempFormat = typeDetections[0]; tempRawValue = rawDetections[0];
                } else if(typeDetections[0]==typeDetections[2] && rawDetections[0].equals(rawDetections[2])){
                    tempFormat = typeDetections[0]; tempRawValue = rawDetections[0];
                } else if(typeDetections[1]==typeDetections[2] && rawDetections[0].equals(rawDetections[1])){
                    tempFormat = typeDetections[1]; tempRawValue = rawDetections[1];
                } else {tempFormat = typeDetections[2]; tempRawValue = rawDetections[2];}

                /**
                 * Typy kodów zwracane są jako int, trzeba je przetworzyć na String w celu zwrócenia
                 * danych. Vision API identyfikuje formaty EAN  następująco: wartość 32 dla EAN13
                 * oraz wartość 64 dla EAN8.
                 */
                String tempFormatTag;
                if(tempFormat == Barcode.EAN_8) {tempFormatTag = "EAN8";}
                else {tempFormatTag = "EAN13";}

                barcodeScannedEventListener.onCodeScanned(tempFormatTag,tempRawValue); //Do zaimplementowania w aktywności nadrzędnej
            }
            else {
                typeDetections[successfulDetectionCount] = item.format;
                rawDetections[successfulDetectionCount] = item.rawValue;
                successfulDetectionCount++; //Kolejna udana detekcja kodu zaliczona
            }
        }

        //Przy zniknięciu z pola widzenia również nie wykonujemy specjalnych akcji
        @Override
        public void onMissing(Detector.Detections<Barcode> detectionResults) {
            //Restart kolejki wykrywania, potrzebne są trzy wyniki jeden po drugim
            successfulDetectionCount = 0;
            //Restart tabel na wszelki wypadek
            typeDetections = new int[3];
            rawDetections = new String[3];
        }

        //Wywoływane przy zniknięciu kodu z pola widzenia
        @Override
        public void onDone() {
            //Tracker zaraz po tym będzie usunięty, nie ma potrzeby nic robić
        }
    }
}