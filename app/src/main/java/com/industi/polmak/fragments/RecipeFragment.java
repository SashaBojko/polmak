package com.industi.polmak.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.industi.polmak.adapters.RecyclerViewRecipeAdapter;
import com.industi.polmak.controllers.DataMediator;

/**
 * Created by alexb on 01.09.2016.
 */

public class RecipeFragment extends Fragment {
    private static final String TAB_POSITION = "tab_position";
    private RecyclerView recyclerView;
    private RecyclerViewRecipeAdapter adapter;

    public static RecipeFragment newInstance(int tabPosition) {
        //Log.i(TAB_POSITION, " = " + tabPosition);
        RecipeFragment fragment = new RecipeFragment();
        Bundle args = new Bundle();
        args.putInt(TAB_POSITION, tabPosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Bundle args = getArguments();
        int tabPosition = args.getInt(TAB_POSITION);
        int category = DataMediator.getRecipeCategories().get(tabPosition).getId();
        recyclerView = new RecyclerView(getActivity());
        /*recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                if (dy > 0) {
                    MainActivity.showOrHideFAB(false);
                    //MainActivity.showOrHideBottomNavigation(false);
                }
                else if (dy < 0){
                    MainActivity.showOrHideFAB(true);
                    //MainActivity.showOrHideBottomNavigation(true);
                }
            }
        });*/
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        adapter = new RecyclerViewRecipeAdapter(getActivity(), DataMediator.getRecipesByCategoryId(category));
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(true);

        return recyclerView;
    }


    public void refresh(){
        if(recyclerView != null && adapter != null) {
            Bundle args = getArguments();
            int tabPosition = args.getInt(TAB_POSITION);
            int category = DataMediator.getRecipeCategories().get(tabPosition).getId();
            //Log.i("Position", "tab position fragment " + tabPosition);
            adapter = new RecyclerViewRecipeAdapter(getActivity(), DataMediator.getRecipesByCategoryId(category));
            recyclerView.setAdapter(adapter);

        }
    }

}
