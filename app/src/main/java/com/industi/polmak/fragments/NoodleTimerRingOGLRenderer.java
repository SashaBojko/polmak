package com.industi.polmak.fragments;


import android.opengl.GLSurfaceView;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by Piotr Nazar on 22.09.2016.
 *
 * Renderer OpenglES 1.0 do tworzenia tarczy timera gotowania makaronów. Jest na sztywno przystosowany
 * do obsługi tej jednej funkcji, i korzysta z kilku podklas do wyświetlenia kropek przesuwających
 * się po tarczy. Ma jedną metodę, pozwalającą
 */

public class NoodleTimerRingOGLRenderer implements GLSurfaceView.Renderer {
    //Finalny budowniczy okręgów (nie ma potrzeby go zmieniać tak długo jak renderer działa)
    private final VertexCircleBuilder vcb = new VertexCircleBuilder();

    //Główny kolor dla tła i "maski" okręgu
    private float[] mainColour = {1.0f,0.0f,0.0f,1.0f};
    //Kolor okręgu prowadzącego dla postępu
    private float[] baseColour = {0.5f,0.0f,0.0f,1.0f};
    //Kolor siatki pokazującej postęp gotowania (domyślnie biały)
    private float[] progressColour = {1.0f,1.0f,1.0f,1.0f};
    //Kolor kółka dla początku gotowania (domyślnie biały)
    private float[] startMarkerColour = {1.0f,1.0f,1.0f,1.0f};
    //Kolor kółka dla gotowania alDente (domyślnie żółty, po zakończeniu domyślnie pomarańczowy)
    private float[] alDenteMarkerColour = {1.0f,1.0f,0.0f,1.0f};

    //Elementy renderowane przez OGL
    private SimpleMesh leadCircle; //Ciemniejszy okrąg "prowadzący" dla postępu gotowania (ciemna czerwień)
    private SimpleMesh progressMesh; //Wycinkowo generowany okrąg, pokazujący postęp
    private SimpleMesh innerCircle; //Okrąg wewnętrzy, maskujący wycinkowy postęp
    private SimpleMesh startMarker; //Znacznik początku gotowania
    private SimpleMesh alDenteMarker; //Znacznik gotowania alDente
    //Znacznik optymalny to de facto znacznik startu!
    //private SimpleMesh optimalMarker; //Znacznik gotowania optymalnego

    //Flaga określająca czy aktualizujemy pierścień postępu
    private boolean ringScheduledToLock = false;
    private boolean ringIsUpdateable = true;

    //Pozycje znacznika aldente - pola do obliczania
    private float[] alDenteMarkerPosition = new float[3]; //Tablica na pozycję

    //Zmiana postępu (aktualizowana przy postępie czasu rzeczywistego
    private float optimalProgress = 0.0f;

    //Konstruktor domyślny (supress unused)
    @SuppressWarnings("unused")
    public NoodleTimerRingOGLRenderer() {
        //Żeby system się nie wywalił, obliczamy prowizorycznie pozycję znacznika aldente na 50% okręgu
        calculateAlDentePosition(50,100);
    }

    //Konstruktor pozwalający skonfigurować renderer zgodnie z potrzebami
    public NoodleTimerRingOGLRenderer(
            //Argumenty konstruktora (dla lepszej czytelności
            float[] mainCol,        //Kolor główny (tło i maskownica)
            float[] baseCol,        //Podstawowy kolor pierścienia postępu
            float[] progCol,        //Kolor aktywnego obszaru pierścienia postepu
            float[] startMarkCol,   //Kolor znacznika startu/końca
            float[] alDentMarkCol,  //Kolor znacznika alDente
            int secAlDent,          //Ile sekund do alDente (tylko do obliczenia pozycji znacznika)
            int secOpti             //Ile sekund do końca gotowania (tylko do obliczenia)
    ) {
        //Podstawienie kolorów
        mainColour = Arrays.copyOf(mainCol,mainCol.length);
        baseColour = Arrays.copyOf(baseCol,baseCol.length);
        progressColour = Arrays.copyOf(progCol,progCol.length);
        startMarkerColour = Arrays.copyOf(startMarkCol,startMarkCol.length);
        alDenteMarkerColour = Arrays.copyOf(alDentMarkCol,alDentMarkCol.length);
        /*
         *  Czasy są potrzebne do obliczenia pozycji znacznika alDente. Ponieważ nie są używane nigdzie
         *  indziej, nie musimy ich zapamiętywać, a całe obliczenia można wykonać tutaj, podczas inicjalizacji.
         */

        calculateAlDentePosition(secAlDent,secOpti);
    }

    /**
     * Wywoływane tylko przy tworzeniu oraz odtworzeniu powierzchni po zmianach orientacji czy skali.
     * Czyszczenie bufora ustawia tło na kolor zdefiniowany przy inicjalizacji. Cieniowanie GL_FLAT
     * jest najszybsze ze wszystkich. Nie potrzebujemy inicjalizować żadnych funkcji dotyczących
     * bufora Z, bo rendering z niego nie korzysta (opiera się wyłącznie na odpowiedniej kolejności.
     *
     * @param gl        GL10        kontekst do renderowania
     * @param config    EGLConfig   konfiguracja OGL, w tym wypadku domyślna
     */
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        gl.glClearColor(mainColour[0],mainColour[1],mainColour[2],mainColour[3]);
        //Cieniowanie płaskie, nie mamy potrzeby uśredniać kolorów
        gl.glShadeModel(GL10.GL_FLAT);

        /*
         *  Siatki dla elementów które się nie zmieniają. 256 wierzchołków powinno dać percepcyjnie
         *  idealne okręgi nawet na dużych ekranach wysokiej rozdzielczości. W razie gdyby się okazało
         *  że narzut jest zbyt wysoki, można zmniejszyć ilość wierzchołków o połowę.
         */
        leadCircle = vcb.buildFilledCircle(256,1.0f,0.0f, baseColour,true);
        innerCircle = vcb.buildFilledCircle(256,0.95f,0.2f,mainColour,true);
        //Znaczniki sekcji na tarczy (pozycjonowane później statycznie w każdej ramce)
        startMarker = vcb.buildFilledCircle(64,0.1f,0.5f,startMarkerColour,true);
        alDenteMarker = vcb.buildFilledCircle(64,0.1f,0.5f,alDenteMarkerColour,true);
    }

    /**
     * Jako że nie korzystamy z testów głębi, kolejność rysowania obiektów ma podstawowe znaczenie
     * dla uzyskania obrazu który wygląda jak trzeba. Brak testów głębi poprawia wydajność (a szczególnie
     * zmniejsza narzut na kontrolerze pamięci), ale wszystkie siatki i tak mają odpowiednie wartości
     * Z zapisane, są również ułożone odpowiednio.
     *
     * @param gl    GL10    interfejs OpenGL ES 1.0 do renderowania
     */
    public void onDrawFrame(GL10 gl) {
        //Wyczyszczenie bufora (czyszczenie głębi usunięte, można łatwo przywrócić w miarę potrzeb)
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT); // | //
                //GL10.GL_DEPTH_BUFFER_BIT);
        //Wyzerowanie transformacji
        gl.glLoadIdentity();
        gl.glPushMatrix(); //Kopia macierzy na stos
        //Rysowanie elementów wyświetlacza
        //Ciemniejszy okrąg, po którym "chodzi" znacznik postępu
        leadCircle.draw(gl);
        //Zbudowanie i narysowanie zaktualizowanej siatki obrazującej postęp gotowania
        if(ringIsUpdateable) {progressMesh = vcb.buildPartialCircle(256,1.0f,0.1f,progressColour,true,optimalProgress);}
        progressMesh.draw(gl); //Rysowany jest zawsze, żeby nie zniknął
        //Okrąg maskujący, dający wygląd pustego koła dla "paska postępu"
        innerCircle.draw(gl);
        //Znacznik startowy
        gl.glTranslatef(0,0.985f,0);
        startMarker.draw(gl); //Marker startowy, na godznie 12
        //Znacznik aldente
        gl.glPopMatrix(); //Przywrócenie oryginalnej macierzy po narysowaniu zniacznika początku
        gl.glTranslatef(alDenteMarkerPosition[0],alDenteMarkerPosition[1],alDenteMarkerPosition[2]);
        alDenteMarker.draw(gl);

        //Po przerysowaniu ramki blokujemy dalsze liczenie postępu
        if(ringScheduledToLock) {ringIsUpdateable = false;}
    }

    /**
     * Obsługa zmiany powierzchni na której renderujemy naszą geometrię. Wykorzystywana jest projekcja
     * ortogonalna, bo jest najbardziej odpowiednia do rysowania elementów interfejsu, plus oszczędza
     * obliczeń związanych z perspektywą.
     *
     * @param gl        GL10    kontekst do renderowania
     * @param width     int     nowa szerokość powierzchni na której renderujemy
     * @param height    int     nowa wysokość powierzchni na któej renderujemy
     */
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        //Zmiana rozmiaru viewportu do obecnego rozmiaru powierzchni
        gl.glViewport(0, 0, width, height);
        //Ustawienie i reset matrycy rzutowania
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        //Inicjalizacja rzutowania ortogonalnego, z niewielkimi marginesami (watości bufora Z z zapasem)
        gl.glOrthof(-1.25f,1.25f,-1.25f,1.25f,-5.0f,5.0f);
        //Matryca modelview i reset tejże
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    //Obliczanie pozycji znacznika aldente z użyciem trygonometrii (pozycja się nie zmienia w trakcie pracy)
    private void calculateAlDentePosition(int aldenteSecondsTime,int optimalSecondsTime) {
        //Wyznaczenie "kąta" znacznika aldente
        float alDenteRadians = ((float) aldenteSecondsTime / (float) optimalSecondsTime) * (float) (2*Math.PI);
        //Obliczenie pozycji, dla promienia równego 88f - dobrane eksperymentalnie
        alDenteMarkerPosition[0] = (float) (0.985f*Math.sin(alDenteRadians));
        alDenteMarkerPosition[1] = (float) (0.985f*Math.cos(alDenteRadians));
        alDenteMarkerPosition[2] = 0.0f; //Brak translacji w osi Z
    }

    /**
     * Ponieważ na tarczy wyświetlamy tylko czas optymalny i aldente, nie ma sensu przekazywać tu
     * czasu łącznego. Przy okazji ten postęp będzie też pokazywał postępy w sekcji al Dente...
     *
     * @param percent   float   procent postępu w gotowaniu optymalnym
     */
    public void updateOptimalPercentage(float percent) {
        optimalProgress = percent;
    }

    /**
     * Bezargumentowa metoda, pozwalająca zawiesić aktualizacje pierścienia postepu po jego zakończeniu.
     * Nie można go później wznowić bez odtworzenia całego renderera!
     */
    public void setProgressRingDisabled() {
        ringScheduledToLock = true;
    }

    /**
     * Created by PNazar on 23.09.2016.
     *
     * Klasa konstruuje okręgi w oparciu tylko o ich promień oraz ilość wierzchołków. Wypełnia też okręgi
     * tworząc ścianki i przygotowuje je do renderowania przez OGL. Transformacje okręgów nie są uwzględniane,
     * to już musi zrobić renderer który z nich korzysta. Można też utworzyć kwadrat z okrężną dziurą wewnątrz
     * (wykorzystywany do rysowania postępu na tarczy).
     *
     * Do tworzenia ścianek używamy metody GL_CW. Kolory nie są definiowane.
     */

    private class VertexCircleBuilder {
        //Tablice wierzchołków i ścianek
        private float meshVertices[];
        private short meshFaces[];

        /**
         * Generuje werteksy okręgu razem z werteksem centralnym. Ilość wierzchołków podana w argumencie
         * nie zawiera wierzchołka centralnego (jest dodawany automatycznie, zawsze na pierwszym miejscu).
         *
         * Każdy wierzchołek to 3 floaty, więc długość tablicy wynosi (numVerts*3)+3.
         *
         * Okręg będzie przygotowany do wypełnienia
         *
         * @param numVerts  int     ilość werteksów jakie ma zawierać okrąg
         * @param rad       float   promień okręgu do wygenerowania
         * @param lay       float   warstwa na której znajduje się siatka (oś z)
         */
        private void createVerticesFullCircle(int numVerts, double rad, float lay) {
            meshVertices = new float[(numVerts*3)+3];
            //Dodanie pierwszego wierzchołka, centralnego, zawsze w pozycji zero
            /*
            for(int coordinate=0;coordinate<2;coordinate++) {
                meshVertices[coordinate]=0.0f; //Punkt centralny
            }
            */
            //Wyliczenie kroku tworzenia łuku, w radianach (potrzebne dla Math)
            //360 stopni = 2Pi radianów (dokładność float wystarczy)
            float radianCircle = (float) (2*Math.PI); //Zachowanie dla wygody
            float radianStep =  radianCircle/numVerts;
            //Obecny kąt
            float radianCurrent = 0.0f;
            //Indeks wierzchołka (od pozycji 3, będzie zwiększany o 3)
            int vertexIndex = 0;
            //Utworzenie kolejnych wierzchołków na obwodzie koła, w równych odstępach
            while(radianCurrent < radianCircle) {
                meshVertices[vertexIndex] = (float) (rad*Math.cos(radianCurrent));
                meshVertices[vertexIndex+1] = (float) (rad*Math.sin(radianCurrent));
                meshVertices[vertexIndex+2] = lay; //Płaski okrąg na podanej warstwie

                //Przeskakujemy do kolejnego wierzchołka
                radianCurrent+=radianStep;
                vertexIndex+=3;
            }
            //Na tym etapie całe koło jest przygotowane do wypełniania ściankami
        }

        /**
         * Podobnie do powyższej metody, ta tworzy wycinki okręgów do wypełnienia. Jako jedyna jest wywoływana
         * w każdym odświeżeniu, dlatego musi wykonywać się szybciej (mniej instrukcji warunkowych).
         * Podawany tu procent to procent pełnego okręgu (wyliczenie dokładnych wymiarów jest gdzieindziej)
         *
         * Wycinkowe koła cierpią na pewne problemy z dokładnością w miarę jak rośnie kąt dla którego obliczamy
         * cos i sin. Najpewniej chodzi o błędy zaokrągleń wynikające z przerabiania wszystkiego na float
         * (zamiast pełnych obliczeń double). W normalnym użytkowaniu to raczej nie będzie problem.
         *
         * @param numVerts      int     ilość wierzchołków jakie miałoby pełne koło
         * @param rad           float   promień tworzonego koła
         * @param lay           float   warstwa na której znajduje się siatka (oś z)
         * @param percentage    float   procent "postępu" wypełnienia okręgu
         */

        private void createVerticesPartialCircle(int numVerts, double rad, float lay, float percentage) {
            meshVertices = new float[(numVerts*3)+3];
            //Dodanie pierwszego wierzchołka, centralnego, zawsze w pozycji zero (jest potrzebny żeby ładnie rozwijać okrąg)
            for(int coordinate=0;coordinate<2;coordinate++) {
                meshVertices[coordinate]=0.0f; //Punkt centralny
            }
            //Wyliczenie kroku tworzenia łuku, w radianach (potrzebne dla Math)
            //Koło tym razem nie ma 360 stopni, a po prostu percentage*2 radianów (wycinek).
            float radianCircle = (float) ((2*Math.PI)* (double) percentage); //Zachowanie dla wygody, cast nie jest potrzebny
            float radianStep =  radianCircle/numVerts;
            //Obecny kąt
            float radianCurrentCut = 0.0f;
            //Indeks wierzchołka (od pozycji 3, będzie zwiększany o 3)
            int vertexIndexCut = 3;
            //Utworzenie kolejnych wierzchołków na obwodzie koła, w równych odstępach
            while(radianCurrentCut < radianCircle-radianStep) {
                //Log.v(LOG_TAG,"Tworzenie wierzchołka "+Integer.toString(vertexIndexCut/3));
                meshVertices[vertexIndexCut] = (float) (rad*Math.sin(radianCurrentCut));
                meshVertices[vertexIndexCut+1] = (float) (rad*Math.cos(radianCurrentCut));
                meshVertices[vertexIndexCut+2] = lay; //Płaski okrąg na podanej warstwie
                //Przeskakujemy do kolejnego wierzchołka
                radianCurrentCut+=radianStep;
                vertexIndexCut+=3;
            }
            //Na tym etapie całe koło jest przygotowane do wypełniania ściankami
        }

        /**
         * Ta metoda generuje ścianki dla wypełnionego okręgu, metodą zgodną z podanym parametrem dotyczącym
         * kolejności (zgodnie z ruchem wskazówek zegara (true) lub przeciwnie (false).
         *
         * Pierwszym punktem ZAWSZE jest punk w centrum okręgu, a kolejne są na jego brzegu
         *
         * @param numVerts  int     ilość wierzchołków na okręgu
         * @param clockwise boolean czy mamy generować ścianki zgodnie, czy przeciwnie do ruchu wskazówek zegara
         */
        private void createFacesFullCircle(int numVerts,boolean clockwise) {
            meshFaces = new short[numVerts*3]; //Inicjalizacja tablicy
            int faceEntryIdx = 0; //Indeks wpisu w tablicy ścianek do którego wstawiamy
            for(int face=0;face<numVerts;face++) {
                //Operujemy na numerach ścianek, nie na numerach wpisów
                if(face==numVerts-1) {
                    //Ostatnia ścianka wypełniana inaczej
                    if(clockwise) {
                        meshFaces[faceEntryIdx] = 0;
                        meshFaces[faceEntryIdx+1] = (short) face;
                        meshFaces[faceEntryIdx+2] = 1;
                    } else {
                        meshFaces[faceEntryIdx] = 0;
                        meshFaces[faceEntryIdx+2] = (short) face;
                        meshFaces[faceEntryIdx+1] = 1;
                    }
                } else {
                    //Wszystkie inne ścianki wypełniane standardowo
                    if(clockwise) {
                        meshFaces[faceEntryIdx] = 0;
                        meshFaces[faceEntryIdx+1] = (short) face;
                        meshFaces[faceEntryIdx+2] = (short) (face+1);
                    } else {
                        meshFaces[faceEntryIdx] = 0;
                        meshFaces[faceEntryIdx+2] = (short) face;
                        meshFaces[faceEntryIdx+1] = (short) (face+1);
                    }
                }
                //Powiększamy indeks wpisu o 3, bo każda ścianka to trzy wpisy
                faceEntryIdx+=3;
            }
        }

        /**
         * Metoda generująca ścianki dla wycinków okręgu. Nie "domyka" ostatniej ścianki automatycznie -
         * generuje po prostu ścianki na kolejnych wierzchołkach.
         *
         * @param numVerts  int     ilość wierzchołków na okręgu
         * @param clockwise boolean czy mamy generować ścianki zgodnie, czy przeciwnie do ruchu wskazówek zegara
         */
        private void createFacesPartialCircle(int numVerts,boolean clockwise) {
            //Log.v(LOG_TAG,"Początek tworzenia " + Integer.toString(numVerts) + " scianek");
            meshFaces = new short[numVerts*3]; //Inicjalizacja tablicy
            //Log.v(LOG_TAG,"Utworzona tablica ścianek ma "+ Integer.toString(meshFaces.length)+" wpisów.");
            int faceEntryIdx = 0; //Indeks wpisu w tablicy ścianek do którego wstawiamy
            for(int face=0;face<numVerts;face++) {
                //Operujemy na numerach ścianek, nie na numerach wpisów
                //Wszystkie ścianki wypełniane standardowo
                if(clockwise) {
                    meshFaces[faceEntryIdx] = 0;
                    meshFaces[faceEntryIdx+1] = (short) face;
                    meshFaces[faceEntryIdx+2] = (short) (face+1);
                } else {
                    meshFaces[faceEntryIdx] = 0;
                    meshFaces[faceEntryIdx+2] = (short) face;
                    meshFaces[faceEntryIdx+1] = (short) (face+1);
                }
                //Powiększamy indeks wpisu o 3, bo każda ścianka to trzy wpisy
                faceEntryIdx+=3;
            }
        }

        SimpleMesh buildFilledCircle(int numVerts, float radius,float layer, float[] cFill, boolean clockwise) {
            createVerticesFullCircle(numVerts,radius, layer);
            createFacesFullCircle(numVerts,clockwise);

            return new SimpleMesh(meshVertices,meshFaces,cFill,clockwise);
        }

        SimpleMesh buildPartialCircle(int numVerts, float radius,float layer, float[] cFill, boolean clockwise, float percentage) {
            createVerticesPartialCircle(numVerts,radius,layer,percentage);
            createFacesPartialCircle(numVerts,clockwise);

            return new SimpleMesh(meshVertices,meshFaces,cFill,clockwise);
        }
    }

    /**
     * Created by Piotr Nazar on 23.09.2016.
     *
     * Klasa zajmuje się przechowywaniem i wyświetlaniem siatek w OGL ES 1.0. W miarę potrzeb można ją
     * rozwinąć o dodatkowe funkcje, póki co działa tylko w zakresie rysowania jednokolorowych obiektów
     */

    private class SimpleMesh {
        //Wierzchołki i ścianki
        private float[] meshVertices;
        private short[] meshFaces;
        private float colourRed;
        private float colourGreen;
        private float colourBlue;
        private float alphaValue;
        private boolean renderClockwise = true; //Czy renderujemy zgodnie, czy przeciwnie do kierunku ruchu wsk. zegara

        //Bufory dla wierzchołków i ścianek
        private FloatBuffer vertexBuffer;
        private ShortBuffer faceBuffer;

        //Konstruktor (domyślny nie jest dostarczany)
        SimpleMesh(float[] mVerts, short[] mFace, float[] cFill,boolean cWise) {
            //Wszystkie argumenty są klonowane na wypadek gdyby oryginały miały "zniknąć"
            meshVertices = mVerts.clone();
            meshFaces = mFace.clone();
            renderClockwise = cWise;
            //Wartości dla koloru (zawsze trzeba przekazać cztery argumenty)
            colourRed=cFill[0];
            colourGreen=cFill[1];
            colourBlue=cFill[2];
            alphaValue=cFill[3];

            //Załadowanie danych do buforów i wyzerowanie pozycji
            ByteBuffer verticesByteBuffer = ByteBuffer.allocateDirect(meshVertices.length * 4);
            verticesByteBuffer.order(ByteOrder.nativeOrder());
            vertexBuffer = verticesByteBuffer.asFloatBuffer();
            vertexBuffer.put(meshVertices);
            vertexBuffer.position(0);

            ByteBuffer facesByteBuffer = ByteBuffer.allocateDirect(meshFaces.length * 2);
            facesByteBuffer.order(ByteOrder.nativeOrder());
            faceBuffer = facesByteBuffer.asShortBuffer();
            faceBuffer.put(meshFaces);
            faceBuffer.position(0);
        }

        /**
         * Tą funkcją możemy ustawić nowy kolor dla siatki (na przykład kiedy zakończy się dany etap,
         * zmieniamy kolor na ciemniejszy lub zwiększamy przeźroczystość). Chwilowo nieużywana,
         * stąd suppress.
         *
         * @param cFill float[] tablica RGBA opisująca nowy kolor danej siatki
         */
        @SuppressWarnings("unused")
        public void setNewColour(float[] cFill) {
            //Wartości dla koloru (zawsze trzeba przekazać cztery argumenty)
            colourRed=cFill[0];
            colourGreen=cFill[1];
            colourBlue=cFill[2];
            alphaValue=cFill[3];
        }

        /**
         * Rysowanie najprostszą możliwą metodą, z użyciem tablic wierzchołków. Nie ma żadnego przesłaniania
         * i obcinania ścianek, więc glFrontFace ma znaczenie najwyżej "porządkowe", ale jest zachowany na
         * wypadek rozszerzania klasy (narzut na CPU jest de facto żaden).
         * @param gl    GL10    kontekst do renderowania
         */
        void draw(GL10 gl) {
            //Wybór kierunku rysowania
            if(renderClockwise) {gl.glFrontFace(GL10.GL_CW);}
            else{gl.glFrontFace(GL10.GL_CCW);}
            //Rendering z użyciem GL_VERTEX_ARRAY
            gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
            //Wskazanie bufora wierzchołków do renderowania
            gl.glVertexPointer(3, GL10.GL_FLOAT, 0,vertexBuffer);
            //Ustawienie koloru renderowania
            gl.glColor4f(colourRed,colourGreen,colourBlue,alphaValue);
            //Narysowanie elementów siatki
            gl.glDrawElements(GL10.GL_TRIANGLES, meshFaces.length,GL10.GL_UNSIGNED_SHORT, faceBuffer);
            //Wyłączenie bufora wierzchołków
            gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        }
    }
}

