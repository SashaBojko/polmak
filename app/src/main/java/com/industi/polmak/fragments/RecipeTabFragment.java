package com.industi.polmak.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.industi.polmak.R;
import com.industi.polmak.adapters.RecipeViewPagerAdapter;

/**
 * Created by alexb on 08.09.2016.
 */

public class RecipeTabFragment extends Fragment {
    private RecipeViewPagerAdapter adapter;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    public RecipeTabFragment() {

    }

    public RecipeTabFragment newInstance(int index) {
        RecipeTabFragment fragment = new RecipeTabFragment();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        this.adapter = new RecipeViewPagerAdapter(getChildFragmentManager());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recipes_fragment_activity, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        init(view);
    }

    private void init(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.recipes_viewpager);
        viewPager.setAdapter(adapter);

        tabLayout = (TabLayout) view.findViewById(R.id.recipes_tablayout);
        tabLayout.setupWithViewPager(viewPager);
    }

}
