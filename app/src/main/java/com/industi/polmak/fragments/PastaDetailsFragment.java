package com.industi.polmak.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.industi.polmak.R;
import com.industi.polmak.activities.MainActivity;
import com.industi.polmak.activities.TimerActivity;
import com.industi.polmak.adapters.RecyclerViewRecipeAdapter;
import com.industi.polmak.basic.Pasta;
import com.industi.polmak.basic.Recipe;
import com.industi.polmak.controllers.DataMediator;
import com.industi.polmak.requests.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by alexb on 08.09.2016.
 */

public class PastaDetailsFragment extends Fragment {
    private Pasta pasta;

    private ImageView image, ingredient1, ingredient2, ingredient3, shape;
    private TextView name, grammage;
    private RecyclerView reciperRecyclerView;
    private NestedScrollView scrollView;
    private ImageView startCookingButton;
    private ImageLoader imageLoader;

    private Toast toast;
    public PastaDetailsFragment() {
        imageLoader = new ImageLoader(getContext());
    }

    public PastaDetailsFragment newInstance(int index) {
        PastaDetailsFragment fragment = new PastaDetailsFragment();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);

        if (DataMediator.getPastas().size() > 0) pasta = DataMediator.getPastas().get(0);
        else pasta = new Pasta();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pasta_details_activity, container, false);

        initUI(view);

        return view;
    }

    private void initUI(View view) {
        image = (ImageView) view.findViewById(R.id.card_view_details_pasta_image);
        ingredient1 = (ImageView) view.findViewById(R.id.card_view_pasta_details_ingredient1_image);
        ingredient1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pasta.getIngredients()[0] != null) {
                    if(toast != null){
                        toast.cancel();
                    }
                    toast.makeText(getContext(), pasta.getIngredients()[0].getName(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        ingredient2 = (ImageView) view.findViewById(R.id.card_view_pasta_details_ingredient2_image);
        ingredient2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pasta.getIngredients()[1] != null){
                    if(toast != null){
                        toast.cancel();
                    }
                    toast.makeText(getContext(), pasta.getIngredients()[1].getName(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        ingredient3 = (ImageView) view.findViewById(R.id.card_view_pasta_details_ingredient3_image);
        ingredient3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pasta.getIngredients()[2] != null){
                    if(toast != null){
                        toast.cancel();
                    }
                    toast.makeText(getContext(), pasta.getIngredients()[2].getName(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        shape = (ImageView) view.findViewById(R.id.card_view_pasta_details_shape_image);
        shape.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(DataMediator.getShapeById(pasta.getShape()) != null){
                    if(toast != null){
                        toast.cancel();
                    }
                    toast.makeText(getContext(), DataMediator.getShapeById(pasta.getShape()).getName(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        grammage = (TextView) view.findViewById(R.id.card_view_pasta_details_grammage);
        name = (TextView) view.findViewById(R.id.card_view_pasta_details_name);
        scrollView = (NestedScrollView) view.findViewById(R.id.pasta_details_scroll_view);
        startCookingButton = (ImageView) view.findViewById(R.id.card_view_makaron_start_cooking);
        startCookingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), TimerActivity.class);
                intent.putExtra("pastaId", pasta.getId());
                startActivity(intent);
            }
        });

        //if scrolled down - hide navigation bar and fab
        //else - show
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY - oldScrollY > 0) {
                    //MainActivity.showOrHideBottomNavigation(false);
                    MainActivity.showOrHideFAB(false);
                } else {
                    //MainActivity.showOrHideBottomNavigation(true);
                    MainActivity.showOrHideFAB(true);
                }
            }
        });

        reciperRecyclerView = (RecyclerView) view.findViewById(R.id.pasta_details_recycler_view);
        reciperRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
        //to smooth scrolling with nestedScrollView
        reciperRecyclerView.setNestedScrollingEnabled(false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
    }

    public void setPasta(Pasta pasta) {
        //Log.i("SELECTED PASTA", pasta.toString());
        this.pasta = pasta;
        if (this.pasta == null) {
            this.pasta = DataMediator.getPastas().get(0);
        }
    }

    public void setData() {
        imageLoader.setImage(image, pasta.getImageLink());
        //Picasso.with(image.getContext()).load(Uri.parse(pasta.getImageLink())).into(image);
        if (pasta.getIngredients()[0] != null) {
            imageLoader.setImage(ingredient1, pasta.getIngredients()[0].getImageLink());
            //Picasso.with(ingredient1.getContext()).load(Uri.parse(pasta.getIngredients()[0].getImageLink())).into(ingredient1);

        }
        if (pasta.getIngredients()[1] != null) {
            imageLoader.setImage(ingredient2, pasta.getIngredients()[1].getImageLink());
            //Picasso.with(ingredient2.getContext()).load(Uri.parse(pasta.getIngredients()[1].getImageLink())).into(ingredient2);

        }
        if (pasta.getIngredients()[2] != null) {
            imageLoader.setImage(ingredient3, pasta.getIngredients()[2].getImageLink());
            //if(pasta.getIngredients()[2]!=null)Picasso.with(ingredient3.getContext()).load(Uri.parse(pasta.getIngredients()[2].getImageLink())).into(ingredient3);
        }
        if (DataMediator.getShapeById(pasta.getShape()) != null)
            imageLoader.setImage(shape, DataMediator.getShapeById(pasta.getShape()).getImageLink());
            //Picasso.with(shape.getContext()).load(Uri.parse(DataMediator.getShapeById(pasta.getShape()).getImageLink())).into(shape);
        grammage.setText(String.valueOf(pasta.getGrammage() + " g"));
        name.setText(pasta.getName());

        updateRecipes();
    }

    //gets recipes with selected pasta from DataMediator
    private void updateRecipes() {
        //when opening fragment - show nav bar and fab (they can be hidden)
        MainActivity.showOrHideFAB(true);
       // MainActivity.showOrHideBottomNavigation(true);

        //add recipes
        ArrayList<Recipe> recipes = DataMediator.getRecipesByPastaId(pasta.getId());
        RecyclerViewRecipeAdapter adapter = new RecyclerViewRecipeAdapter(getActivity(), recipes);
        reciperRecyclerView.setAdapter(adapter);
    }

}
