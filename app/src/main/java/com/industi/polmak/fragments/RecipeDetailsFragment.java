package com.industi.polmak.fragments;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.industi.polmak.R;
import com.industi.polmak.activities.MainActivity;
import com.industi.polmak.adapters.RecyclerViewIngredientsAdapter;
import com.industi.polmak.basic.Ingredient;
import com.industi.polmak.basic.Recipe;
import com.industi.polmak.controllers.DataMediator;
import com.industi.polmak.requests.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by alexb on 08.09.2016.
 */

public class RecipeDetailsFragment extends Fragment {
    private Recipe recipe;

    private ImageView image;
    private TextView name, time, energyValue, price, level, description, cookingMethod;
    private ImageView addPhotoImage;
    private Button addPhotoButton;

    private RecyclerView ingridients;
    private ArrayList<Ingredient> ingridientsList;
    private RecyclerViewIngredientsAdapter adapter;

    private ImageLoader imageLoader;
    private LoginButton loginButton;

    public RecipeDetailsFragment() {
        imageLoader = new ImageLoader(getContext());
    }

    public RecipeDetailsFragment newInstance(int index) {

        RecipeDetailsFragment fragment = new RecipeDetailsFragment();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);

        if(DataMediator.getRecipes().size() > 0) recipe = DataMediator.getRecipes().get(0);
        else recipe = new Recipe();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recipe_details_activity, container, false);

        initUI(view);
        setShareButtonvisible();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
    }

    @Override
    public void onStop() {
        adapter.saveSelectedIngredients();
        super.onStop();
    }

    private void initUI(View view) {
        initIngredientsRecyclerView(view);
        image = (ImageView)view.findViewById(R.id.recipe_details_image);
        name = (TextView)view.findViewById(R.id.recipe_details_name);
        time = (TextView)view.findViewById(R.id.recipe_details_time);
        energyValue = (TextView)view.findViewById(R.id.recipe_details_energy_value);
        price = (TextView)view.findViewById(R.id.recipe_details_price);
        level = (TextView)view.findViewById(R.id.recipe_details_level);
        description = (TextView)view.findViewById(R.id.recipe_details_description);
        cookingMethod = (TextView)view.findViewById(R.id.recipes_details_method_of_cooking_description);
        addPhotoImage = (ImageView) view.findViewById(R.id.recipe_details_add_photo_image);
        addPhotoButton = (Button) view.findViewById(R.id.recipe_details__add_photo_button);
        loginButton = (LoginButton)view.findViewById(R.id.recipe_details__login_fb);
        loginButton.setPublishPermissions("publish_actions");
        loginButton.registerCallback(((MainActivity)getActivity()).getFbManager(), new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Toast.makeText(getActivity(), "LogedIn!", Toast.LENGTH_SHORT).show();
                setShareButtonvisible();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getActivity(), "Cancel", Toast.LENGTH_SHORT).show();
                setShareButtonvisible();
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                setShareButtonvisible();
            }
        });
        View.OnClickListener addPhotoListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //do something to add photo
                if(((MainActivity)getActivity()).isLoggedIn()){
                    ((MainActivity)getActivity()).makePhoto();
                }else{
                    Toast.makeText(getContext(), "Najpierw zaloguj się", Toast.LENGTH_SHORT).show();
                }

            }
        };
        addPhotoImage.setOnClickListener(addPhotoListener);
        addPhotoButton.setOnClickListener(addPhotoListener);
    }

    private void initIngredientsRecyclerView(View view) {
        ingridients = (RecyclerView) view.findViewById(R.id.recipe_details_ingredients);
        ingridients.setNestedScrollingEnabled(false);
        ingridients.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    public void setRecipe(Recipe recipe){
        this.recipe = recipe;
    }

    private void setData(){
        imageLoader.setImage(image, recipe.getImageLink());
        name.setText(recipe.getName());
        time.setText(getHoursFromMinutes(recipe.getTime()));
        energyValue.setText(String.valueOf(recipe.getEnergyValue() + " kcal"));
        price.setText(String.valueOf(recipe.getPrice() + " zł/os"));
        level.setText(getLevelName(recipe.getDifficulty()));
        description.setText(recipe.getDescription());
        cookingMethod.setText(recipe.getPreparation());

        updateIngridients();
    }

    private void updateIngridients(){
        ingridientsList = recipe.getIngredients();
        adapter = new RecyclerViewIngredientsAdapter(getContext(), recipe, ingridientsList);
        ingridients.setAdapter(adapter);
    }

    private String getHoursFromMinutes(int minutes) {
        if (minutes < 60) {
            return minutes + " min";
        } else {
            int hours = minutes / 60;
            int m = minutes % 60;
            if (m != 0) {
                return hours + " godz " + m + " min";
            } else {
                return hours + " godz";
            }
        }
    }

    private String getLevelName(int level){
        switch (level){
            case 1: return "Łatwe";
            case 2: return "Średnie";
            default: return "Trudne";
        }
    }

    private void setShareButtonvisible(){
        if(((MainActivity)getActivity()).isLoggedIn()){
            loginButton.setVisibility(View.INVISIBLE);
        }else{
            loginButton.setVisibility(View.VISIBLE);
        }
    }


}
