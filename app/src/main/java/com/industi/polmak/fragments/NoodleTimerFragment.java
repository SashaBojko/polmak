package com.industi.polmak.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 *
 * Created by Piotr Nazar on 06.09.2016.
 *
 * Bezgłowy fragment zajmujący się logiką czasów gotowania. Pozwala uzyskać czas do zakończenia każdego
 * etapu gotowania oraz jego procentowy postęp. Ma też funkcję do zapisania czasu łącznego, na wypadek
 * gdyby użytkownik chciał go zapisać jako czas użytkownika.
 *
 * Timer pracuje po zminimalizowaniu aplikacji, ale nie po jej całkowitym zamknięciu. Nie generuje
 * powiadomień jako takich (tym musi zająć się aktywność która dostaje z niego dane).
 *
 * Z założenia wszystkie czasy wejściowe (i wyjściowe) są w sekundach, a wewnętrznie operujemy na
 * milisekundach (tak jak realtime clock).
 */

public class NoodleTimerFragment extends Fragment {
    //Całkowity czas gotowania (z wyłączeniem pauz)
    private int totalTimeElapsedSec =0;

    //Czasy trwania liczników w milisekundach
    long timerAlDenteDurationMSec;
    long timerOptimalDurationMSec;
    long timerUserDurationMSec;

    //Oryginalne czasy trwania liczników w milisekundach
    long timerAlDenteDurationMSecOriginal;
    long timerOptimalDurationMSecOriginal;
    long timerUserDurationMSecOriginal;

    //Automatycznie zapisywane czasy liczników (w każdym ticku)
    long timerAlDenteUntilDoneMSec=0;
    long timerOptimalUntilDoneMSec=0;
    long timerUserUntilDoneMSec=0;

    //Lista czasomierzy aktywnych we fragmencie
    CountDownTimer noodleTimerAlDente;
    CountDownTimer noodleTimerOptimal;
    CountDownTimer noodleTimerUser;
    CountDownTimer totalTimeCounter;

    //Flaga określająca, czy dane wywołanie jest wynikiem restartu czy wznowienia
    boolean isBeingRestarted = false; //Domyślnie tworzymy nowe timery

    public OnFragmentInteractionListener noodleTimerInteractionListener;

    public NoodleTimerFragment() {/*Wymagany pusty konstruktor*/}

    /**
     * Fabryka do wytwarzania fragmentów. Nie przyjmuje argumentów w bundle z uwagi na fakt,
     * że kolejkowanie alarmów odbywa się z użyciem osobnych, publicznych metod.
     */
    public static NoodleTimerFragment newInstance() {
        return new NoodleTimerFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return null; //Brak interfejsu dla tego fragmentu
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            noodleTimerInteractionListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " musi zaimplementować OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //Wyzerowanie nasłuchu (w celu uniknięcia wywołań na nieistniejących obiektach)
        noodleTimerInteractionListener = null;
    }

    /**
     * Interfejs OnFragmentInteractionListener służy do komunikacji fragmentu z używającą go aktywnością.
     * Po zaimplementowaniu przez nią odpowiednich metod możliwe jest zwracanie czasów z liczników
     * oraz reagowanie na zakończenie odliczania dla każdego etapu gotowania
     */
    public interface OnFragmentInteractionListener {
        //Metody odpalane co 1 sekundę w celu aktualizacji liczników w aplikacji
        void onAlDenteTimerTick(int secondsRemaining, float progress);
        void onOptimalTimerTick(int secondsRemaining, float progress);
        void onUserTimerTick(int secondsRemaining, float progress);

        //Metody odpalane na koniec każdego z timerów (bez argumentów)
        void onAlDenteTimerFinished();
        void onOptimalTimerFinished();
        void onUserTimerFinished();

        //Metoda do aktualizacji czasu bieżącego
        void onElapsedTimeTick(int totalElapsedTime);

        //Wywołanie po zakończeniu czasu elapsed, ustawionego na 1 godzinę (ubicie timerów)
        void onElapsedTimeFinished();
    }

    /**
     * =============================================================================================
     * Publiczne metody do ustawiania poszczególnych alarmów. Są osobne, dla zachowania kontroli nad tym,
     * co i gdzie zostało zakolejkowane (listy byłyby problematyczne i wprowadzały niepotrzebnie dużo
     * dodatkowego kodu do zarządzania nimi).
     *
     * Każda metoda przyjmuje tylko jeden parametr - długość danego etapu gotowania (licząc od startu wszystkich liczników).
     *
     * Wszystkie metody onTick i onFinish mają zabezpieczenie przed wywoływaniem ich na pustej referencji
     * do obiektu nasłuchującego. Zapobiega to błędom w sytuacji gdy użytkownik nagle zamknie aktywność
     * bez zatrzymywania zegarów.
     */

    public void addAlDenteTimer(int durationSeconds) {
        timerAlDenteDurationMSec = (long) durationSeconds*1000;
        if(!isBeingRestarted) {timerAlDenteDurationMSecOriginal = (long) durationSeconds*1000;} //Tylko jeśli to jest nowe utworzenie
        //Ustawienie licznika w dół
        noodleTimerAlDente = new CountDownTimer(timerAlDenteDurationMSec,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if(noodleTimerInteractionListener !=null) {
                    //Zapisanie wartości czasu
                    timerAlDenteUntilDoneMSec = millisUntilFinished;
                    //Wyliczenie postepu procentowego (od 0.0f -> początek do 1.0f -> koniec odliczania)
                    float sectionProgress = ((((float) millisUntilFinished / (float) timerAlDenteDurationMSecOriginal)) - 1.0f) * (-1.0f);
                    //Zwraca ilość sekund do końca licznika i postęp według powyższego
                    noodleTimerInteractionListener.onAlDenteTimerTick((int) (millisUntilFinished / 1000), sectionProgress);
                }
            }
            @Override
            public void onFinish() {
                if(noodleTimerInteractionListener !=null) {
                    //Zakończenie licznika
                    noodleTimerInteractionListener.onAlDenteTimerFinished();
                }
            }
        };
    }

    public void addOptimalTimer(int durationSeconds) {
        timerOptimalDurationMSec = (long) durationSeconds*1000;
        if(!isBeingRestarted) {timerOptimalDurationMSecOriginal = (long) durationSeconds*1000;} //Tylko jeśli to jest nowe utworzenie
        //Ustawienie licznika w dół
        noodleTimerOptimal = new CountDownTimer((long) durationSeconds*1000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if(noodleTimerInteractionListener !=null) {
                    //Zapisanie wartości czasu
                    timerOptimalUntilDoneMSec = millisUntilFinished;
                    //Wyliczenie postepu procentowego (od 0.0f -> początek do 1.0f -> koniec odliczania)
                    float sectionProgress = ((((float) millisUntilFinished / (float) timerOptimalDurationMSecOriginal)) - 1.0f) * (-1.0f);
                    //Zwraca ilość sekund do końca licznika
                    noodleTimerInteractionListener.onOptimalTimerTick((int) (millisUntilFinished / 1000), sectionProgress);
                }
            }
            @Override
            public void onFinish() {
                if(noodleTimerInteractionListener !=null) {
                    //Zakończenie licznika
                    noodleTimerInteractionListener.onOptimalTimerFinished();}
            }
        };
    }

    public void addUserTimer(int durationSeconds) {
        timerUserDurationMSec = (long) durationSeconds*1000;
        if(!isBeingRestarted) {timerUserDurationMSecOriginal = (long) durationSeconds*1000;} //Tylko jeśli to jest nowe utworzenie
        //Ustawienie licznika w dół
        noodleTimerUser = new CountDownTimer((long) durationSeconds*1000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if(noodleTimerInteractionListener !=null) {
                    //Zapisanie wartości czasu
                    timerUserUntilDoneMSec = millisUntilFinished;
                    //Wyliczenie postepu procentowego (od 0.0f -> początek do 1.0f -> koniec odliczania)
                    float sectionProgress = ((((float) millisUntilFinished / (float) timerUserDurationMSecOriginal)) - 1.0f) * (-1.0f);
                    //Zwraca ilość sekund do końca licznika
                    noodleTimerInteractionListener.onUserTimerTick((int) (millisUntilFinished / 1000), sectionProgress);
                }
            }
            @Override
            public void onFinish() {
                if(noodleTimerInteractionListener !=null) {
                    //Zakończenie licznika
                    noodleTimerInteractionListener.onUserTimerFinished();}
            }
        };
    }

    /**
     * =============================================================================================
     * Licznik liczący w dół i samoczynnie się odnawiający, dla symulowania liczenia czasu łącznego.
     * W normalnych warunkach nie powinien się przekręcić, ale mechanizm jest dodany na wszelki wypadek.
     */
    private void addTotalTimeCounter() {
        if(totalTimeCounter != null) {totalTimeCounter.cancel(); totalTimeCounter = null;}
        totalTimeCounter = new CountDownTimer(1800000,1000) { //Po pół godziny i tak już będzie po ptokach ;)
            @Override
            public void onTick(long millisUntilFinished) {
                if(noodleTimerInteractionListener != null) {
                    //Co tick dodajemy sekundę do czasu łącznego
                    totalTimeElapsedSec++;
                    noodleTimerInteractionListener.onElapsedTimeTick(totalTimeElapsedSec);
                }
            }

            @Override
            public void onFinish() {
                if(noodleTimerInteractionListener != null) {
                    //To też jest tick, więc dodajemy sekundę normalnie
                    totalTimeElapsedSec++;
                    //noodleTimerInteractionListener.onElapsedTimeTick(totalTimeElapsedSec);
                    //addTotalTimeCounter(); //Samoczynne odnowienie zegara
                    //totalTimeCounter.start(); //Autostart samego siebie

                    //Wywołanie metody na koniec godziny (najpewniej skasowanie wszystkich liczników)
                    noodleTimerInteractionListener.onElapsedTimeFinished();
                }
            }
        };
    }

    // =============================================================================================
    //Metoda startująca wszystkie ustawione liczniki czasu od obecnie ustalonych wartości
    public void startAllTimers() {
        //Bezwarunkowo licznik czasu łącznego
        addTotalTimeCounter();

        //Start istniejących czasomierzy
        if(noodleTimerAlDente != null) {noodleTimerAlDente.start();}
        if(noodleTimerOptimal != null) {noodleTimerOptimal.start();}
        if(noodleTimerUser != null) {noodleTimerUser.start();}
        totalTimeCounter.start(); //Start bezwarunkowy

        //Ustawienie flagi do wznowień
        isBeingRestarted = true;
    }

    //Tutaj można spauzować wszystkie liczniki i wrócić do nich potem
    public void pauseAllTimers() {
        //Skasowanie wszystkich obecnych liczników (wartości czasów są automatycznie zapisywane co krok)
        if(noodleTimerAlDente != null) {noodleTimerAlDente.cancel();}
        if(noodleTimerOptimal != null) {noodleTimerOptimal.cancel();}
        if(noodleTimerUser != null) {noodleTimerUser.cancel();}
        if(totalTimeCounter != null) {totalTimeCounter.cancel();}
        //Po tej metodzie trzeba wykonać resumeAllTimers() żeby wrócić do punktu w którym się zatrzymaliśmy
    }

    //Metoda pozwala wznowić odliczanie tam gdzie zostało spauzowane
    public void resumeAllTimers() {
        if(timerAlDenteUntilDoneMSec>0) {addAlDenteTimer((int) (timerAlDenteUntilDoneMSec / 1000));}
        if(timerOptimalUntilDoneMSec>0) {addOptimalTimer((int) (timerOptimalUntilDoneMSec / 1000));}
        if(timerUserUntilDoneMSec>0) {addUserTimer((int) (timerUserUntilDoneMSec / 1000));}
        //Licznik czasu łącznego startuje bezwarunkowo
        addTotalTimeCounter();
        startAllTimers(); //Wystartowanie liczników
    }

    //Metoda pozwala zrestartować timery do poziomu domyślnego bez startu
    public void resetAllTimers() {
        //Zatrzymanie timerów
        //pauseAllTimers();
        if(noodleTimerAlDente != null) {noodleTimerAlDente.cancel();}
        if(noodleTimerOptimal != null) {noodleTimerOptimal.cancel();}
        if(noodleTimerUser != null) {noodleTimerUser.cancel();}
        if(totalTimeCounter != null) {totalTimeCounter.cancel();}
        //Odtworzenie timerów z domyślnym czasem
        if(timerAlDenteDurationMSecOriginal>0){addAlDenteTimer((int) (timerAlDenteDurationMSecOriginal / 1000));}
        if(timerOptimalDurationMSecOriginal>0){addOptimalTimer((int) (timerOptimalDurationMSecOriginal / 1000));}
        if(timerUserDurationMSecOriginal>0) {addUserTimer((int) (timerUserDurationMSecOriginal / 1000));}
        //Licznik czasu łącznego dodawany jest podczas startu
        //Resetuje też łączny czas gotowania
        totalTimeElapsedSec = 0;
    }


    //Metoda zatrzymuje wszystkie timery i dodaje czas do puli łącznej, po czym zwraca łączny czas
    public int stopAllTimersGetElapsed() {
        //Zatrzymanie wszystkich liczników
        if(noodleTimerAlDente != null) {noodleTimerAlDente.cancel();}
        if(noodleTimerOptimal != null) {noodleTimerOptimal.cancel();}
        if(noodleTimerUser != null) {noodleTimerUser.cancel();}
        //Zwrócenie czasu
        return totalTimeElapsedSec;
    }

    // =============================================================================================
    // Akcesor dla czasu całkowitego, na wszelki wypadek (suppress)
    @SuppressWarnings("unused")
    public int getElapsedTime() {return totalTimeElapsedSec;}
}
