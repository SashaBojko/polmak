package com.industi.polmak.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.industi.polmak.R;
import com.industi.polmak.activities.MainActivity;

/**
 * Created by alexb on 08.09.2016.
 */
public class ScanResultFragment extends Fragment {
    private Button scanAgainButton;
    public ScanResultFragment(){

    }

    public ScanResultFragment newInstance(int index){
        ScanResultFragment fragment = new ScanResultFragment();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View fragmentView = inflater.inflate(R.layout.barcode_result_activity, container, false);
        scanAgainButton = (Button) fragmentView.findViewById(R.id.barcode_result_scan_again_button);
        scanAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).startBarcodeActivity();
            }
        });
        return fragmentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }
}
