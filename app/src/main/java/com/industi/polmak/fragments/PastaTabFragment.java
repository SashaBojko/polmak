package com.industi.polmak.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.industi.polmak.R;
import com.industi.polmak.adapters.PastaViewPagerAdapter;

public class PastaTabFragment extends Fragment {
    private PastaViewPagerAdapter adapter;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    public PastaTabFragment() {

    }

    public PastaTabFragment newInstance(int index) {
        PastaTabFragment fragment = new PastaTabFragment();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        this.adapter = new PastaViewPagerAdapter(getChildFragmentManager());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pasta_fragment_activity, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        init(view);
    }

    private void init(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.makaron_viewpager);
        viewPager.setAdapter(adapter);

        tabLayout = (TabLayout) view.findViewById(R.id.makaron_tablayout);
        tabLayout.setupWithViewPager(viewPager);
    }

}
