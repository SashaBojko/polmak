package com.industi.polmak.fragments;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.industi.polmak.activities.MainActivity;
import com.industi.polmak.adapters.RecyclerViewPastaAdapter;
import com.industi.polmak.controllers.DataMediator;


public class PastaFragment extends Fragment {
    private static final String TAB_POSITION = "tab_position";
    private static final String LIST_STATE = "tab_position";
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private Parcelable listState;

    public PastaFragment() {

    }

    public PastaFragment newInstance(int tabPosition) {
        PastaFragment fragment = new PastaFragment();
        Bundle args = new Bundle();
        args.putInt(TAB_POSITION, tabPosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Log.i("ONCREATE", "PASTAFRAGMENT");
        //layoutManager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);

        recyclerView = new RecyclerView(getActivity());
        //recyclerView.setLayoutManager(layoutManager);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Bundle args = getArguments();
        int tabPosition = args.getInt(TAB_POSITION);
        int seriesId = DataMediator.getSeries().get(tabPosition).getId();

        //Log.i(TAB_POSITION, "position = " + tabPosition);
        setRecyclerViewLayoutManager();
        recyclerView.setFocusable(true);
        recyclerView.setClickable(true);

        RecyclerViewPastaAdapter adapter = new RecyclerViewPastaAdapter(getActivity(), DataMediator.getPastasBySeriesId(seriesId));
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0)
                    MainActivity.showOrHideFAB(false);
                else if (dy < 0)
                    MainActivity.showOrHideFAB(true);
            }
        });

        return recyclerView;
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        //Log.i("onRestoreState", "restore");
        if (savedInstanceState != null) {
            listState = savedInstanceState.getParcelable(LIST_STATE);
            recyclerView.getLayoutManager().onRestoreInstanceState(listState);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstantState) {
        super.onSaveInstanceState(savedInstantState);

        //Log.i("onSaveState", "save");

        savedInstantState.putParcelable(LIST_STATE, recyclerView.getLayoutManager().onSaveInstanceState());
    }

    public void setRecyclerViewLayoutManager() {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (recyclerView.getLayoutManager() != null) {
            scrollPosition = ((GridLayoutManager) recyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        layoutManager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.scrollToPosition(scrollPosition);
    }

}
