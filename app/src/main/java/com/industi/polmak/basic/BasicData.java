package com.industi.polmak.basic;

/**
 * Created by alexb on 19.09.2016.
 */

public class BasicData {
    protected int id;
    protected String name, imageLink;

    public BasicData() {
        id = 0;
        name = "";
        imageLink = "";
    }

    public BasicData(String name) {
        this.name = name;
    }

    public BasicData(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public BasicData(int id, String name, String imageLink) {
        this.id = id;
        this.name = name;
        this.imageLink = imageLink;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }
}
