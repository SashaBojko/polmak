package com.industi.polmak.basic;

/**
 * Created by alexb on 09.09.2016.
 */

public class Ingredient extends BasicData{

    public Ingredient(int id, String description, String iconLink) {
        super(id,description,iconLink);
    }

    public Ingredient(String description) {
        super(description);
    }

}
