package com.industi.polmak.basic;

import java.util.ArrayList;

/**
 * Created by alexb on 01.09.2016.
 */

public class Recipe extends BasicData{
    private int id;
    private String imageLink;
    private String title, preparation, description;
    private ArrayList<Ingredient> ingredients;
    private int time, energyValue, price, pastaId, pastaCategoryId, difficulty;

    public Recipe(){
        super();
        this.preparation = "";
        this.description = "";
        this.ingredients = null;
        this.time = 0;
        this.energyValue = 0;
        this.price = 0;
        this.pastaId = 0;
        this.pastaCategoryId = 0;
        this.difficulty = 0;
    }
    public Recipe(int id, String imageLink, String title, String preparation, String description, ArrayList<Ingredient> ingredients, int time, int energyValue, int price, int pastaId, int pastaCategoryId, int difficulty) {
        super(id,title,imageLink);
        this.preparation = preparation;
        this.description = description;
        this.ingredients = ingredients;
        this.time = time;
        this.energyValue = energyValue;
        this.price = price;
        this.pastaId = pastaId;
        this.pastaCategoryId = pastaCategoryId;
        this.difficulty = difficulty;
    }

    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getEnergyValue() {
        return energyValue;
    }

    public void setEnergyValue(int energyValue) {
        this.energyValue = energyValue;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPastaId() {
        return pastaId;
    }

    public void setPastaId(int pastaId) {
        this.pastaId = pastaId;
    }

    public int getPastaCategoryId() {
        return pastaCategoryId;
    }

    public void setPastaCategoryId(int pastaCategoryId) {
        this.pastaCategoryId = pastaCategoryId;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }
}
