package com.industi.polmak.basic;

/**
 * Created by alexb on 31.08.2016.
 */
public class Pasta extends BasicData{
    private int shape, series, alDenteTime, boiledTime, grammage;
    private Ingredient[] ingredients;
    private String eanCode;

    public Pasta() {
        super();
        this.shape = 1;
        this.series = 1;
        this.alDenteTime = 0;
        this.boiledTime = 30;
        this.grammage = 300;
        this.ingredients = null;
        this.eanCode = "";
    }

    public Pasta(int id, String name, String imageLink, int shape, int series, int alDenteTime, int boiledTime, int grammage, String eanCode, Ingredient[] ingredients) {
        super(id, name, imageLink);
        this.shape = shape;
        this.series = series;
        this.alDenteTime = alDenteTime;
        this.boiledTime = boiledTime;
        this.grammage = grammage;
        this.ingredients = ingredients;
        this.eanCode = eanCode;
    }

    public int getShape() {
        return shape;
    }

    public void setShape(int shape) {
        this.shape = shape;
    }

    public int getSeries() {
        return series;
    }

    public void setSeries(int series) {
        this.series = series;
    }

    public int getAlDenteTime() {
        return alDenteTime;
    }

    public void setAlDenteTime(int alDenteTime) {
        this.alDenteTime = alDenteTime;
    }

    public int getBoiledTime() {
        return boiledTime;
    }

    public void setBoiledTime(int boiledTime) {
        this.boiledTime = boiledTime;
    }

    public int getGrammage() {
        return grammage;
    }

    public void setGrammage(int grammage) {
        this.grammage = grammage;
    }

    public String getEanCode() {
        return eanCode;
    }

    public void setEanCode(String eanCode) {
        this.eanCode = eanCode;
    }

    public Ingredient[] getIngredients() {
        return ingredients;
    }

    public void setIngredients(Ingredient[] ingredients) {
        for (int i = 0; i < ingredients.length; i++) {
            this.ingredients[i] = ingredients[i];
        }
    }
}
