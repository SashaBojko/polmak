package com.industi.polmak.basic;

/**
 * Created by alexb on 13.09.2016.
 */

public class RecipeCategory extends BasicData {

    public RecipeCategory(int id, String name) {
        super(id,name);
    }
}
