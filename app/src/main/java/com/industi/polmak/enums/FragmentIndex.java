package com.industi.polmak.enums;

/**
 * Created by alexb on 08.09.2016.
 */

public class FragmentIndex {
    public static final int PastaTabFragment = 0;
    public static final int ScanResultFragment = 1;
    public static final int RecipeTabFragment = 2;
    public static final int PastaDetailsFragment = 3;
    public static final int RecipeDetailsFragment = 4;
}
