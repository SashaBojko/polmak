package com.industi.polmak.enums;

/**
 * Created by alexb on 08.08.2016.
 */
public class URLS {
    public static final String BASIC_URL = "http://api-app-polmak.deployflex.com/";

    public static final String URL_GET_PASTAS = BASIC_URL + "pastas";
    public static final String URL_GET_RECIPES = BASIC_URL + "recipes";
    public static final String URL_GET_SHAPES = BASIC_URL + "shapes";
    public static final String URL_GET_SERIES = BASIC_URL + "series";
    public static final String URL_GET_RECIPE_CATEGORIES = BASIC_URL + "categories";
    public static final String URL_POST_TIME = BASIC_URL + "cook-times";
}
